# The Template for SUpost-like applications

## For deployment
We need to change configurations:

### S3 Bucket
Edit config/s3.yml file

### Site Configuration: WuFoo Forms, Site name, contact email, allowed domains, logo_url, ...
Edit config/site_config.yml

by Greg Wientjes and Phong Nguyen


============

June 7, 2015

OLD:

# Restart all applications in a group (server)
bundle exec cap production group:restart GROUP=<group>
# Restart all applications in all groups (servers)
bundle exec cap production group:restart:all


NEW:

# Restart all applications in a group (server)
bundle exec cap production group:aio:restart GROUP=<group>
# Restart all applications in all groups (servers)
bundle exec cap production group:aio:restart:all


==========

SUpost Clone Deploy Method:

- Add password for deploy user

1qazxsw2

- Add password for deploy user for postgresql

d3p!0y

- Update nginx config

worker_processes from "auto" to "1"

- Setup deployment structure for new droplet

bundle exec cap production deploy:check

- Create database

createdb -U deploy gtechlist

- Run deployment

bundle exec cap production deploy APP=gtechlist

- Setup initial data for app (this is run once)

bundle exec cap production deploy:seed APP=gtechlist

- Restart the application server

bundle exec cap production deploy:seed APP=gtechlist

- Setup crontab for 3tabs

crontab -e

- Deploy lavishlevity.com use beaverwall config

1. Add config (site config and 3taps)
cp config/sites/beaverwall.yml config/sites/lavishlevity.yml
cp config/3taps/beaverwall.yml config/3taps/lavishlevity.yml

2. Update lib/capistrano/helpers.rb

Include app "lavishlevity" to server (GROUP 7) which app will be run on
APPLICATIONS = {
# trojy.com
1 => %w[ beaverwall ... ],

# BuckEyesList.com
2 => %w[ huwall ... ],

# gtownlist.com
3 => %w[ aggiepost ... ],

# tigerswall.com
4 => %w[ huskerlist ... ],

# wildcatlist.com
5 => %w[ uaalist ... ],

# sydpost.com
6 => %w[ aucklist ... ],

7 => %w[ gtechlist lavishlevity ]
}

3. Log into server and create database for lavishlevity
createdb -U deploy lavishlevity

4. Deploy the new clone
bundle exec cap production deploy APP=lavishlevity
bundle exec cap production deploy:seed APP=lavishlevity # (run once when deploying it first time)
bundle exec cap production deploy:restart APP=lavishlevity
bundle exec cap production deploy:restart_nginx APP=lavishlevity

5. Copy anchor for lavishlevity

bundle exe cap production anchor:create APP=lavishlevity ANCHOR=2200404205

6. Log into server and Setup crontab
crontab -e


- Crontab for gtechlist is failed because of missing anchor
- copy anchor from existing app
- This is run once when the new clone is added

bundle exec cap production anchor APP=berkelist

DEBUG [7a9738f6] Running /usr/bin/env [ ! -d ~/.rbenv/versions/2.0.0-p643 ] on 198.199.110.148
DEBUG [7a9738f6] Command: [ ! -d ~/.rbenv/versions/2.0.0-p643 ]
DEBUG [7a9738f6] Finished in 4.196 seconds with exit status 1 (failed).
DEBUG [2ecdadeb] Running /usr/bin/env if test ! -d /home/deploy/berkelist/shared; then echo "Directory does not exist '/home/deploy/berkelist/shared'" 1>&2; false; fi on 198.199.110.148
DEBUG [2ecdadeb] Command: if test ! -d /home/deploy/berkelist/shared; then echo "Directory does not exist '/home/deploy/berkelist/shared'" 1>&2; false; fi
DEBUG [2ecdadeb] Finished in 0.486 seconds with exit status 0 (successful).
INFO [47d369d4] Running /usr/bin/env cat config/anchor.txt on 198.199.110.148
DEBUG [47d369d4] Command: cd /home/deploy/berkelist/shared && ( RBENV_ROOT=~/.rbenv RBENV_VERSION=2.0.0-p643 /usr/bin/env cat config/anchor.txt )
DEBUG [47d369d4] 2200404205
INFO [47d369d4] Finished in 0.458 seconds with exit status 0 (successful).

bundle exe cap production anchor:create APP=gtechlist ANCHOR=2200404205


- Deploy new clone nascentnexus.com -- use trojy.com

1. Add config (site config and 3taps)
cp config/sites/trojy.yml config/sites/nascentnexus.yml
cp config/3taps/trojy.yml config/3taps/nascentnexus.yml

2. Update lib/capistrano/helpers.rb

Include app "nascentnexus" to server (GROUP 7) which app will be run on
APPLICATIONS = {
# trojy.com
1 => %w[ beaverwall ... ],

# BuckEyesList.com
2 => %w[ huwall ... ],

# gtownlist.com
3 => %w[ aggiepost ... ],

# tigerswall.com
4 => %w[ huskerlist ... ],

# wildcatlist.com
5 => %w[ uaalist ... ],

# sydpost.com
6 => %w[ aucklist ... ],

7 => %w[ gtechlist lavishlevity nascentnexus ]
}

3. Log into server and create database for nascentnexus.yml
createdb -U deploy nascentnexus

4. Deploy the new clone
bundle exec cap production deploy APP=nascentnexus
bundle exec cap production deploy:seed APP=nascentnexus # (run once when deploying it first time)
bundle exec cap production deploy:restart APP=nascentnexus
bundle exec cap production deploy:restart_nginx APP=nascentnexus

5. Copy anchor for nascentnexus

bundle exe cap production anchor:create APP=nascentnexus ANCHOR=2200404205

6. Log into server and Setup crontab
crontab -e