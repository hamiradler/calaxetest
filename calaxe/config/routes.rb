XPost::Application.routes.draw do
  root :to => "pages#home"

  namespace :admin do
    root :to => "posts#index"
    resources :posts do
      collection do
        get :overview
        get :imported
      end
    end

    resources :errors, :only => [:index, :show, :edit, :destroy] do
      collection{ post :remove }
    end

    resources :import_settings, :only => [:index]
    resources :settings, :only => [:index, :update]

    match 'login'  => "sessions#new",     :as => :login,  :via => :get
    match 'login'  => "sessions#create",  :as => :login,  :via => :post
    match 'logout' => "sessions#destroy", :as => :logout, :via => :get
  end

  controller "pages",  :path => "/" do
    match 'contact', :action => :contact, :via => :get
    match 'about',   :action => :about,   :via => :get
    match 'privacy', :action => :privacy, :via => :get
    match 'terms',   :action => :terms,   :via => :get
    match 'help',    :action => :help,    :via => :get
  end

  resources :posts do
    member do
      get   :preview
      post  :continue
      match "publish/:security_id", :action => :publish, :via => :get, :as => :publish
      match "delete/:security_id",  :action => :delete,  :via => :get, :as => :delete
    end

    collection do
      get   :add
      match "add/:cat",        :action => :new,    :via => :get
      match "add/:cat(/:sub)", :action => :new,    :via => :get
      match "add/:cat/:sub",   :action => :create, :via => :post

      match "new/:cat",        :action => :new,    :via => :get
      match "new/:cat(/:sub)", :action => :new,    :via => :get, :as => :add
      match "new/:cat/:sub",   :action => :create, :via => :post

      get :instructions
      get :featured
    end

    resources :messages, :only => [:create]
  end

  resources :jobs, :only => [:index] do
    collection do
      get :subscriber
      get :unsubscribe
      get :success
    end
  end

  match "job"            => "jobs#index"
  match "job/subscriber" => "jobs#subscriber"
  match "unsubscribe"    => "jobs#unsubscribe"
  match "success"        => "jobs#success"


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => "welcome#index"

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
