# config valid only for Capistrano 3.1
lock '3.1.0'

set :rbenv_ruby,  ENV['RBENV_VERSION'] || '2.0.0-p643'
set :user,        'deploy'

set :application, 'cornellpost'
set :repo_url,    'git@bitbucket.org:hamiradler/calaxetest.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# Default deploy_to directory is /var/www/my_app
set :deploy_to, "/home/#{user}/#{application}"

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 3

set :ssh_options, { forward_agent: true }

desc 'Print current anchor of current application'
task :anchor do
  on roles(:app) do
    within shared_path do
      execute :cat, "config/anchor.txt"
    end
  end
end

desc 'Create anchor for current application'
task :'anchor:create' do
  anchor = ENV['ANCHOR'].to_s.dup.strip
  if anchor.empty?
    puts "ANCHOR must not be blank"
    exit 1
  end

  on roles(:app) do
    within shared_path do
      execute :echo, "#{anchor} > config/anchor.txt"
    end
  end
end

namespace :deploy do
  desc 'Create database'
  task :createdb do
    on roles(:db) do
      within '~' do
        execute :createdb, "-U `whoami` #{app_database}" rescue nil
      end
    end
  end

  desc "Remove unused columns"
  task :remove_unused_columns do
    on roles(:app), in: :sequence, wait: 5 do
      within release_path do
        execute :rake, "db:remove_unused_columns", "RAILS_ENV=#{rails_env}"
      end
    end
  end

  desc 'Seed data'
  task :seed do
    on roles(:app), in: :sequence, wait: 5 do
      within release_path do
        execute :rake, 'db:seed', "RAILS_ENV=#{rails_env}"
      end
    end
  end

  ['start', 'stop', 'restart'].each do |action|
    desc "#{action.capitalize} Ruby web server"
    task action.to_sym do
      on roles(:app), in: :sequence, wait: 5 do
        execute puma_script_path, action
      end
    end
  end

  desc 'Stop current application - remove nginx config and puma init script, then restart nginx'
  task :shutdown do
    on roles(:app), in: :sequence, wait: 5 do
      execute puma_script_path, 'stop'

      # Unicorn
      # sudo :rm, "-fv #{unicorn_nginx_config_path}"
      # sudo :rm, "-fv #{unicorn_script_path}"

      # Puma
      sudo :rm, "-fv #{puma_nginx_config_path}"
      sudo :rm, "-fv #{puma_script_path}"
    end

    invoke 'deploy:restart_nginx'
  end

  desc 'Restart current application - copy nginx config and puma init script, then restart nginx'
  task :relaunch do
    on roles(:app), in: :sequence, wait: 5 do
      # Unicorn
      # sudo :cp, "-fv #{release_path}/config/#{unicorn_nginx_config} #{unicorn_nginx_config_path}"
      # sudo :cp, "-fv #{release_path}/config/#{unicorn_script}       #{unicorn_script_path}"

      # Puma
      sudo :cp, "-fv #{release_path}/config/#{puma_nginx_config} #{puma_nginx_config_path}"
      sudo :cp, "-fv #{release_path}/config/#{puma_script}       #{puma_script_path}"

      execute puma_script_path, 'start'
    end

    invoke 'deploy:restart_nginx'
  end

  desc 'Restart nginx'
  task :restart_nginx do
    on roles(:app), in: :sequence, wait: 5 do
      sudo nginx_script_path, 'restart'
    end
  end

  desc 'Setup configs'
  task :setup do
    on roles(:app) do
      within shared_path do
        execute :mkdir, '-pv config'
        execute :touch, 'config/anchor.txt'
        execute :echo,  'Current anchor: $(cat config/anchor.txt)'
      end

      within release_path do
        execute :ln, "-sfv #{shared_path}/config/anchor.txt config/anchor.txt"

        template "amazon_s3.yml.erb",      "config/amazon_s3.yml"
        template "database.yml.erb",       "config/database.yml"
        template "3taps.sh.erb",           "config/#{threetaps_cron}"
        template "parse.sh.erb",           "config/#{parse_cron}"

        # Unicorn
        # template "nginx_unicorn.conf.erb", "config/#{unicorn_nginx_config}"
        # template "unicorn_init.sh.erb",    "config/#{unicorn_script}"
        # template "unicorn.rb.erb",         "config/unicorn.rb"

        # Puma
        template "nginx_puma.conf.erb",    "config/#{puma_nginx_config}"
        template "puma_init.sh.erb",       "config/#{puma_script}"
        template "puma.rb.erb",            "config/puma.rb"

        # Crons
        execute :chmod, "755 config/#{threetaps_cron}"
        execute :chmod, "755 config/#{parse_cron}"

        # Unicorn
        # execute :chmod, "755 config/#{unicorn_nginx_config}"
        # execute :chmod, "755 config/#{unicorn_script}"

        # Puma
        execute :chmod, "755 config/#{puma_nginx_config}"
        execute :chmod, "755 config/#{puma_script}"

        execute :mkdir, "-pv #{threetaps_cron_dir}"
        execute :mkdir, "-pv #{parse_cron_dir}"

        execute :cp, "-fv config/3taps/#{app_config} config/3taps.yml"
        execute :cp, "-fv config/sites/#{app_config} config/site_config.yml"
        execute :cp, "-fv config/#{threetaps_cron}   #{threetaps_cron_path}"
        execute :cp, "-fv config/#{parse_cron}       #{parse_cron_path}"

        # Unicorn
        # sudo :cp, "-fv config/#{unicorn_nginx_config} #{unicorn_nginx_config_path}"
        # sudo :cp, "-fv config/#{unicorn_script}       #{unicorn_script_path}"

        # Puma
        sudo :cp, "-fv config/#{puma_nginx_config} #{puma_nginx_config_path}"
        sudo :cp, "-fv config/#{puma_script}       #{puma_script_path}"
      end
    end
  end

  after :updating,   :setup
  after :publishing, :restart

  # after :restart, :clear_cache do
  #   on roles(:web), in: :groups, limit: 3, wait: 10 do
  #     # Here we can do anything such as:
  #     within release_path do
  #       execute :rake, 'cache:clear'
  #     end
  #   end
  # end

end

namespace :rbenv do
  desc 'Upgrade rbenv and rbenv plugins'
  task :upgrade do
    on roles(:app) do
      rbenv_root    = File.join('~', '.rbenv')
      rbenv_plugins = File.join(rbenv_root, 'plugins')

      within rbenv_root do
        execute :git, 'reset --hard'
        execute :git, 'pull --rebase origin master'
      end

      plugins = capture(:ls, rbenv_plugins).split("\n")

      plugins.each do |plugin|
        within File.join(rbenv_plugins, plugin) do
          execute :git, 'reset --hard'
          execute :git, 'pull --rebase origin master'
        end
      end
    end
  end

  desc 'Install a Ruby version, VERSION=<version>'
  task :install do |t, args|
    on roles(:app) do
      version = ENV['VERSION'].to_s.dup.strip

      if version.empty?
        error "Please specify ruby version to install"
        error "Use: rbenv:install VERSION=<version>"
        exit 1
      end

      execute :mkdir, '-p ~/.rbenv/cache'

      options = []
      if ENV['FORCE']
        options << "--force"
      else
        options << "--skip-existing"
      end
      options << "--verbose" if ENV["VERBOSE"]
      options = options.join(" ")

      cmd = "RBENV_ROOT=~/.rbenv RUBY_CONFIGURE_OPTS=--disable-install-doc ~/.rbenv/bin/rbenv install #{options} #{version}"
      execute cmd
    end
  end

  desc 'Install Bundler VERSION=<version> for RBENV_VERSION=<version>'
  task :bundler do
    on roles(:app) do
      version = ENV['VERSION'].to_s.dup.strip

      options = ["--no-document"]
      options << "-v #{version.inspect}" unless version.empty?
      options = options.join(" ")

      cmd = "RBENV_ROOT=~/.rbenv RBENV_VERSION=#{fetch(:rbenv_ruby)} ~/.rbenv/bin/rbenv exec gem install bundler #{options}"
      execute cmd
    end
  end

  desc 'Uninstall all Bundler gems for RBENV_VERSION=<version>'
  task :uninstall_bundler do
    on roles(:app) do
      cmd = "RBENV_ROOT=~/.rbenv RBENV_VERSION=#{fetch(:rbenv_ruby)} ~/.rbenv/bin/rbenv exec gem uninstall bundler --all --force"
      execute cmd
    end
  end

  desc "Set Ruby version for RBENV_VERSION=<version>"
  task :set do
    on roles(:app) do
      rbenv = "RBENV_ROOT=~/.rbenv ~/.rbenv/bin/rbenv"

      execute "#{rbenv} local  #{fetch(:rbenv_ruby)}"
      execute "#{rbenv} global #{fetch(:rbenv_ruby)}"

      execute "#{rbenv} local"
      execute "#{rbenv} global"
    end
  end
end

namespace :logs do
  desc 'View rails log'
  task :rails do
    trap("INT"){ puts 'Interupted'; exit 0; }
    on roles(:app) do
      within release_path do
        execute :tail, "-f log/#{rails_env}.log"
      end
    end
  end

  ['puma', 'unicorn', '3taps'].each do |action|
    desc "View #{action}.log"
    task action.to_sym do
      trap("INT"){ puts 'Interupted'; exit 0; }

      on roles(:app), in: :sequence, wait: 5 do
        within release_path do
          execute :tail, "-f log/#{action}.log"
        end
      end
    end
  end

  desc 'Clear logs'
  task :clear do
    on roles(:app) do
      within release_path do
        execute :truncate, "--size 0 log/#{rails_env}.log"
        execute :truncate, "--size 0 log/3taps.log"
        execute :truncate, "--size 0 log/puma.log"

        execute :rm, "-f log/#{rails_env}.log.*"
        execute :rm, "-f log/3taps.log.*"
        execute :rm, "-f log/puma.log.*"
      end
    end
  end
end
