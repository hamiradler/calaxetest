#!/usr/bin/env ruby

PGBACKUP = File.expand_path File.join("~", "pgbackup")

now   = Time.now
YEAR  = now.strftime("%Y")
MONTH = now.strftime("%m")

apps = Dir[ File.join(PGBACKUP, "*") ].reject { |e| ! File.directory?(e) }.sort
apps = apps.map { |a| a.split("/").last }

apps.each do |app|
  puts "=> App: #{app}"
  files = Dir[ File.join(PGBACKUP, app, "*") ].reject { |e| ! File.file?(e) }.sort

  files.each do |file|
    name  = file.split("/").last
    md    = name.match(/^.*-(\d{4})(\d{2})\d{2}\.sql\.gz$/) || []
    year  = md[1] || YEAR
    month = md[2] || MONTH

    cmd = "/usr/local/bin/gsutil -m cp #{file} 'gs://supost-clones-backup/#{app}/#{year}/#{month}/#{name}'"
    puts "-> Running: #{cmd}"
    system cmd
    puts
  end
end
