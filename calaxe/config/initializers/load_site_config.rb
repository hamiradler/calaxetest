config = YAML.load_file("#{Rails.root}/config/site_config.yml")

# SMTP settings
smtp_settings = config.delete("smtp_settings")
if smtp_settings.blank?
  smtp_settings = {
    :user_name            => "wientjes",
    :password             => "asf4565a",
    :domain               => "calaxe.com",
    :address              => "smtp.sendgrid.net",
    :port                 => 587,
    :authentication       => :plain,
    :enable_starttls_auto => true
  }
else
  smtp_settings = smtp_settings.symbolize_keys
end

ActionMailer::Base.smtp_settings = smtp_settings

# Attach brand name of website to noreply email
config[:noreply_email] = "#{config['site_name']}<#{config['short_noreply_email']}>"

SITE_CONFIG = config.with_indifferent_access
