config.parse_options!

set :vps,         config.server

set :branch,      'master'
set :rails_env,   'production'
set :application, config.application
set :deploy_to,   "/home/#{user}/#{application}"

# Used in nginx config
set :domain,      config.domain

server vps, user: user, roles: %w{app web db}, primary: true

before 'rbenv:validate', 'validate_group!' do
  tasks = Rake.application.top_level_tasks
  tasks = tasks[1..-1]

  group_tasks = %w[deploy restart aio:restart seed createdb anchor anchor:create backup delete_backups check logs:clear remove_unused_columns]
  group_tasks = group_tasks.map { |t| "group:#{t}" }

  if tasks.any? { |t| group_tasks.include?(t) }
    config.validate_group!
  end

  anchor_tasks = %w[group:anchor:create group:anchor:createall]

  if tasks.any? { |t| anchor_tasks.include?(t) }
    config.validate_anchor!
  end
end

before 'rbenv:validate', 'show_application_info' do
  puts "* Application: #{application.inspect}, Server: #{vps.inspect}"
end

namespace :group do
  desc "Remove unused columns in all applications for all groups"
  task :"remove_unused_columns:all" do
    trap('INT') { puts 'Interupted'; exit 1 }

    run_locally do
      config.groups.each do |group|
        cmd = config.build_command("group:remove_unused_columns", :GROUP => group)
        puts "* Running #{cmd.inspect}"
        system(cmd)
      end
    end
  end

  desc "Remove unused columns in all applications in a group, GROUP=<group>"
  task :remove_unused_columns do
    trap('INT') { puts 'Interupted'; exit 1 }

    config.validate_group!

    run_locally do
      config.applications.each do |app|
        cmd = config.build_command("deploy:remove_unused_columns", :APP => app)
        puts "* Running #{cmd.inspect}"
        system(cmd)
      end
    end
  end

  desc "Clear logs for all groups"
  task :"logs:clear:all" do
    trap('INT') { puts 'Interupted'; exit 1 }

    run_locally do
      config.groups.each do |group|
        cmd = config.build_command("group:logs:clear", :GROUP => group)
        puts "* Running #{cmd.inspect}"
        system(cmd)
      end
    end
  end

  desc "Clear logs for all applications in a group, GROUP=<group>"
  task :"logs:clear" do
    trap("INT") { puts "Interupted"; exit 1 }

    config.validate_group!

    on roles(:app) do
      within '~' do
        # Clear pgbackup log
        execute :mkdir, "-p pgbackup"
        execute :truncate, "--size 0 pgbackup/backup.log"
        execute :rm, "-f pgbackup/backup.log.*"

        config.applications.each do |app|
          execute :truncate, "--size 0 #{app}/shared/log/#{rails_env}.log"
          execute :truncate, "--size 0 #{app}/shared/log/3taps.log"
          execute :truncate, "--size 0 #{app}/shared/log/puma.log"

          execute :rm, "-f #{app}/shared/log/#{rails_env}.log.*"
          execute :rm, "-f #{app}/shared/log/3taps.log.*"
          execute :rm, "-f #{app}/shared/log/puma.log.*"
        end
      end
    end
  end

  desc 'Deploy all groups'
  task 'deploy:all'.to_sym do
    trap('INT') { puts 'Interupted'; exit 1 }

    run_locally do
      config.groups.each do |group|
        cmd = config.build_command('group:deploy', :GROUP => group, :NGINX => ENV["NGINX"])
        puts "* Running #{cmd.inspect}"
        system(cmd)
      end
    end
  end

  desc 'Deploy all applications in a group, GROUP=<group>'
  task :deploy do
    trap('INT') { puts 'Interupted'; exit 1 }

    config.validate_group!

    run_locally do
      config.applications.each do |app|
        cmd = config.build_command('deploy', :APP => app)
        puts "* Running #{cmd.inspect}"
        system(cmd)
      end

      # Restart Nginx by default
      unless ENV["NGINX"] == "0"
        cmd = config.build_command('deploy:restart_nginx', :APP => config.applications.first)
        puts "* Running #{cmd.inspect}"
        system(cmd)
      end
    end
  end

  desc 'Restart all groups'
  task 'restart:all'.to_sym do
    trap('INT') { puts 'Interupted'; exit 1 }

    run_locally do
      config.groups.each do |group|
        cmd = config.build_command('group:restart', :GROUP => group, :NGINX => ENV['NGINX'])
        puts "* Running #{cmd.inspect}"
        system(cmd)
      end
    end
  end

  desc 'Restart all applications in a group, GROUP=<group>'
  task :restart do
    trap('INT') { puts 'Interupted'; exit 1 }

    config.validate_group!

    run_locally do
      # Restart Nginx by default
      unless ENV['NGINX'] == '0'
        cmd = config.build_command('deploy:restart_nginx', :APP => config.applications.first)
        puts "* Running #{cmd.inspect}"
        system(cmd)
        sleep 5
      end

      config.applications.each do |app|
        cmd = config.build_command('deploy:restart', :APP => app)
        puts "* Running #{cmd.inspect}"
        system(cmd)
      end
    end
  end

  desc 'Restart all groups more faster'
  task :"aio:restart:all" do
    trap('INT') { puts 'Interupted'; exit 1 }

    run_locally do
      config.groups.each do |group|
        cmd = config.build_command('group:aio:restart', :GROUP => group, :NGINX => ENV['NGINX'])
        puts "* Running #{cmd.inspect}"
        system(cmd)
      end
    end
  end

  desc 'Restart all applications in a group more faster, GROUP=<group>'
  task :"aio:restart" do
    trap('INT') { puts 'Interupted'; exit 1 }

    config.validate_group!

    on roles(:app) do
      config.applications.each do |app|
        execute "/etc/init.d/puma-#{app}", "restart"
      end

      unless ENV['NGINX'] == '0'
        execute "/etc/init.d/nginx", "restart"
      end
    end
  end

  desc 'Seed data for all groups'
  task 'seed:all'.to_sym do
    trap('INT') { puts 'Interupted'; exit 1 }

    run_locally do
      config.groups.each do |group|
        cmd = config.build_command('group:seed', :GROUP => group)
        puts "* Running #{cmd.inspect}"
        system(cmd)
      end
    end
  end

  desc 'Seed data for all application in a group, GROUP=<group>'
  task :seed do
    trap('INT') { puts 'Interupted'; exit 1 }

    config.validate_group!

    run_locally do
      config.applications.each do |app|
        cmd = config.build_command('deploy:seed', :APP => app)
        puts "* Running #{cmd.inspect}"
        system(cmd)
      end
    end
  end

  desc 'Create database for all groups'
  task 'createdb:all'.to_sym do
    trap('INT') { puts 'Interupted'; exit 1 }

    run_locally do
      config.groups.each do |group|
        cmd = config.build_command('group:createdb', :GROUP => group)
        puts "* Running #{cmd.inspect}"
        system(cmd)
      end
    end
  end

  desc 'Create database for all applications in a group, GROUP=<group>'
  task :createdb do
    trap('INT') { puts 'Interupted'; exit 1 }

    config.validate_group!

    on roles(:db) do
      within '~' do
        config.databases.each do |database|
          execute(:createdb, "-U `whoami` #{database}") rescue nil
        end
      end
    end
  end

  desc 'Show anchor for all groups'
  task 'anchor:all'.to_sym do
    trap('INT') { puts 'Interupted'; exit 1 }

    run_locally do
      config.groups.each do |group|
        cmd = config.build_command('group:anchor', :GROUP => group)
        puts "* Running #{cmd.inspect}"
        system(cmd)
      end
    end
  end

  desc 'Show anchor for all applications in a group'
  task :anchor do
    trap('INT') { puts 'Interupted'; exit 1 }

    config.validate_group!

    on roles(:app) do
      within '~' do
        cmd = config.applications.map do |app|
          path = File.join(app, 'current', 'config', 'anchor.txt')
          "echo #{app}: $(cat #{path})"
        end.join(';')

        execute :sh, '-c', %Q("#{cmd}")
      end
    end
  end

  desc 'Create initial anchor for all groups'
  task 'anchor:createall'.to_sym do
    trap('INT') { puts 'Interupted'; exit 1 }

    config.validate_anchor!

    run_locally do
      config.groups.each do |group|
        cmd = config.build_command('group:anchor:create', :GROUP => group, :ANCHOR => config.anchor)
        puts "* Running #{cmd.inspect}"
        system(cmd)
      end
    end
  end

  desc 'Create initial anchor for all applications in a group, GROUP=<group>'
  task 'anchor:create'.to_sym do
    trap('INT') { puts 'Interupted'; exit 1 }

    config.validate_group!
    config.validate_anchor!

    on roles(:app) do
      config.applications.each do |app|
        within File.join('~', app, 'current') do
          execute :echo, "#{config.anchor} > config/anchor.txt"
        end
      end
    end
  end

  desc 'Generate backup script for all groups'
  task :"backup:all" do
    trap('INT') { puts 'Interupted'; exit 1 }

    run_locally do
      config.groups.each do |group|
        cmd = config.build_command("group:backup", :GROUP => group)
        puts "* Running #{cmd.inspect}"
        system(cmd)
      end
    end
  end

  desc 'Generate backup script for a group, GROUP=<group>'
  task :backup do
    trap('INT') { puts 'Interupted'; exit 1 }

    config.validate_group!

    on roles(:app) do
      execute :mkdir, "-p ~/pgbackup"

      within release_path do
        template 'backup.sh.erb',         'backup.sh'
        template "delete-backups.sh.erb", "delete-backups.sh"

        execute  :mv, "-vf backup.sh ~/pgbackup"
        execute  :mv, "-vf delete-backups.sh ~/pgbackup"
        execute  :cp, "-vf config/templates/upload.rb ~/pgbackup"
      end

      execute :chmod, "755 ~/pgbackup/backup.sh"
      execute :chmod, "755 ~/pgbackup/delete-backups.sh"
      execute :chmod, "755 ~/pgbackup/upload.rb"
    end
  end

  desc 'Check website status for all groups'
  task 'check:all'.to_sym do
    trap('INT') { puts 'Interupted'; exit 1 }

    run_locally do
      config.groups.each do |group|
        cmd = config.build_command('group:check', :GROUP => group)
        puts "* Running #{cmd.inspect}"
        system(cmd)
      end
    end
  end

  desc 'Check website status for a group, GROUP=<group>'
  task :check do
    trap('INT') { puts 'Interupted'; exit 1 }

    config.validate_group!

    run_locally do
      config.websites.each do |site|
        cmd  = config.open_program % site
        puts "* Running #{cmd.inspect}"
        `sleep 2`
        `#{cmd}`
      end
    end
  end

  desc "Upgrade rbenv and plugins on all servers"
  task "rbenv:upgrade" do
    trap('INT') { puts 'Interupted'; exit 1 }

    run_locally do
      config.groups.each do |group|
        cmd = config.build_command('rbenv:upgrade', :GROUP => group)
        puts "* Running #{cmd.inspect}"
        system cmd
      end
    end
  end

  desc "Install a Ruby version on all servers, VERSION=<version>"
  task :"rbenv:install" do |t, args|
    on roles(:app) do
      version = ENV['VERSION'].to_s.dup.strip

      if version.empty?
        error "Please specify ruby version to install"
        error "Use: group:rbenv:install VERSION=<version>"
        exit 1
      end

      run_locally do
        config.groups.each do |group|
          cmd = config.build_command("rbenv:install", :GROUP => group, :VERSION => version)
          puts "* Running #{cmd.inspect}"
          system cmd
        end
      end
    end
  end

  desc "Install Bundler VERSION=<version> for RBENV_VERSION=<version>"
  task :"rbenv:bundler" do
    on roles(:app) do
      run_locally do
        config.groups.each do |group|
          cmd = config.build_command("rbenv:bundler", :GROUP => group, :VERSION => ENV['VERSION'], :RBENV_VERSION => ENV['RBENV_VERSION'])
          puts "* Running #{cmd.inspect}"
          system cmd
        end
      end
    end
  end

  desc "Uninstall all Bundler gems for RBENV_VERSION=<version>"
  task :"rbenv:uninstall_bundler" do
    on roles(:app) do
      run_locally do
        config.groups.each do |group|
          cmd = config.build_command("rbenv:uninstall_bundler", :GROUP => group, :RBENV_VERSION => ENV['RBENV_VERSION'])
          puts "* Running #{cmd.inspect}"
          system cmd
        end
      end
    end
  end

  desc "Set Ruby version for RBENV_VERSION=<version>"
  task :"rbenv:set" do
    on roles(:app) do
      run_locally do
        config.groups.each do |group|
          cmd = config.build_command("rbenv:set", :GROUP => group, :RBENV_VERSION => ENV['RBENV_VERSION'])
          puts "* Running #{cmd.inspect}"
          system cmd
        end
      end
    end
  end
end
