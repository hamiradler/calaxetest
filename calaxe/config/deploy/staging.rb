set :vps,         '198.199.110.148'

set :branch,      'staging'
set :rails_env,   'staging'
set :application, 'calaxe-staging'
set :deploy_to,   "/home/#{user}/#{application}"

# Used in nginx config
set :domain,      'staging.berkelist.com'

server vps, user: user, roles: %w{app web db}, primary: true
