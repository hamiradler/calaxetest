require 'spec_helper'

describe "PostsSearches" do
  
  it "should allow me to look through the posts with query text search" do
    
    # names = ["my bike", "isn't my niece the cutest ever!!!", "my house"]
    
    loadPosts 400 #, names
    
    visit root_path
    fill_in :q,         :with => "cutest"
    click_button
    response.should render_template('posts/index')
    response.body.should match /<h1>'cutest' searched<\/h1>/
    response.body.should have_selector('a', :content => "cutest", 
                                            :count => 100)
    click_link "cutest"
    response.should render_template('posts/show')
    response.should have_selector('title', :content => 'cutest')
    response.should have_selector('h1', :content => 'cutest')
    response.body.should have_selector("div[class='body_post']", 
                                       :content => "niece")
    fill_in :q,         :with => "cutest"
    click_button
    response.should render_template('posts/index')
    response.body.should match /<h1>'cutest' searched<\/h1>/
    response.body.should have_selector('a', 
                                :content => "isn't my niece the cutest ever!!!", 
                                :count => 100)
    click_link "Next 100 Posts"
    response.should render_template('posts/index')
    response.body.should match /<h1>'cutest' searched<\/h1>/
    response.body.should have_selector('a', :content => "cutest")   
    click_link "Previous"
    response.should render_template('posts/index')
    response.body.should match /<h1>'cutest' searched<\/h1>/
    response.body.should have_selector('a', :content => "cutest")   
    fill_in :q,         :with => "no_posts_will_match_this_query"
    click_button
    response.should render_template('posts/index')
    response.should have_selector('h1', 
                                      :content => "I couldn't find any posts.")
  end
  
  # it "should allow navigation with breadcrumbs" 
end