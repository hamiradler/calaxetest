require 'spec_helper'

describe "Posts" do

  describe "post process" do

    describe "failure" do

      it "should not make a new post" do
        lambda do
          visit new_post_path
          response.should render_template 'posts/categories'
          click_link "for sale / wanted"
          response.should render_template 'posts/subcategories'
          click_link "cars"
          response.should render_template 'posts/new'
          fill_in "Post Title",   :with => ""
          fill_in "Post Description",   :with => ""
          fill_in "Your #{SITE_CONFIG[:uni]} Email",   :with => ""
          click_button "Preview"
          response.should render_template 'posts/new'
          response.should have_selector('div#error_explanation')
        end.should_not change(Post, :count)
      end
    end

    describe "success" do

      post_title = "My Bike"
      post = { :title => "My Bike", :body => "My bike is for sale!",
                                    :email => "gwientjes@gmail.com" }

      it "should make a new post" do
        lambda do
          visit new_post_path
          response.should render_template 'posts/categories'
          response.should have_selector('h1',
                                      :content => "What type of post is this?")
          click_link "for sale / wanted"
          response.should render_template 'posts/subcategories'
          response.should have_selector('h1',
                                      :content => "Please choose a category:")
          click_link "bicycles"
          response.should render_template 'posts/new'
          response.should have_selector('label', :content => "Post Title")
          fill_in "Post Title",   :with => post[:title]
          fill_in "Post Description",   :with => post[:body]
          fill_in "Your #{SITE_CONFIG[:uni]} Email",   :with => post[:email]
          click_button "Preview"
          response.should have_selector('h1', :content => post[:title] )
          response.body.should match post[:body]
          response.should render_template 'posts/preview'
          click_button "Continue"
          response.should have_selector('h2',
                              :content => "You will receive an email shortly,")
          response.should render_template 'posts/instructions'
        end.should change(Post, :count).by(1)
      end
    end
  end
end

