require 'spec_helper'

describe "LayoutLinks" do
  it "should have a Home page at '/' " do
    get '/'
    response.should_not have_selector('title', :content => "|")
    response.should have_selector('title', :content => SITE_CONFIG[:site_name])
  end

  it "should have a Contact page at '/contact" do
    get '/contact'
    response.should have_selector('title', :content => "Contact")
  end

  it "should have an About page at '/about'" do
    get '/about'
    response.should have_selector('title', :content => "About")
  end

  it "should have an About page at '/about'" do
    get '/privacy'
    response.should have_selector('title', :content => "Privacy")
  end

  it "should have an About page at '/about'" do
    get '/terms'
    response.should have_selector('title', :content => "Terms")
  end

  it "should have an About page at '/about'" do
    get '/help'
    response.should have_selector('title', :content => "Help")
  end

  it "should have a post page at '/posts/add" do
    get '/posts/add'
    response.should have_selector('title', :content => "Post")
  end

  it "should have a posts (search) page at /posts" do
    get '/posts'
    response.should have_selector('title', :content => "Posts")
  end


  it "should have the right links on the layout" do
    visit root_path
    response.should have_selector('title', :content => SITE_CONFIG[:site_name])
    response.should_not have_selector('title', :content => "|")
    click_link "about"
    response.should have_selector('title', :content => "About")
    click_link "privacy"
    response.should have_selector('title', :content => "Privacy")
    click_link "contact"
    response.should have_selector('title', :content => "Contact")
    click_link "terms"
    response.should have_selector('title', :content => "Terms")
    click_link "help"
    response.should have_selector('title', :content => "Help")
    click_link /^post$/
    response.should have_selector('title', :content => "Post")
    response.should have_selector('a[href="/"]>img')
    click_link SITE_CONFIG[:site_name]
    response.should have_selector('title', :content => SITE_CONFIG[:site_name])
    response.should_not have_selector('title', :content => "|")
    click_link SITE_CONFIG[:university]
    response.should have_selector('title', :content => SITE_CONFIG[:site_name])
    response.should_not have_selector('title', :content => "|")

  end
end

