require "spec_helper"

describe NotifierMailer do
  it 'should have access to URL helpers' do
    lambda { posts_url }.should_not raise_error
  end

  describe ".activate" do
    before(:each) do
      time_posted = Time.now.to_i
      @time = Time.at(time_posted).strftime("%a %b %d, %Y %I:%M %p")
      email = "test@#{SITE_CONFIG[:allowed_domains].first}"
      @post = mock_model(Post, {
                :id => 1,
                :email => email,
                :name => "My Post",
                :time_posted => time_posted,
                :security_id => 9999
              })

      @post.stub!(:time).and_return(@time)
      @post.stub!(:security_id).and_return(9999)
      @post.stub!(:id).and_return(1)
      @mail = NotifierMailer.activate(@post)
    end

    it 'should render the subject email' do
      @mail.subject.should == "#{SITE_CONFIG[:site_name]} - Publish your post! #{@post.name}"
    end

    it 'should render the sender' do
      @mail.from.should == [SITE_CONFIG[:short_noreply_email]]
    end

    it 'should render the receiver email' do
      Rails.env.stub!(:production?).and_return(true)
      NotifierMailer.activate(@post).to.should == [@post.email]
    end

    it 'should render "nhphong1406@gmail.com" email in staging and development environments' do
      [:staging?, :development?].each do |env|
        Rails.env.stub!(env).and_return(true)
        NotifierMailer.activate(@post).to.should == ['nhphong1406@gmail.com']
      end
    end

    #ensure that the post.time appears in the email body
    it 'should assign post.time' do
      @mail.body.encoded.should match(@time)
    end

    #ensure that the post.name appears in the email body
    it 'should assign post.name' do
      @mail.body.encoded.should match(@post.name)
    end

    #ensure that the @url variable appears in the email body
    it 'should have publish post url' do
      @mail.body.encoded.should match(publish_post_url(@post, :security_id => @post.security_id))
    end

  end

  describe ".msg" do
    before :each do
      time_posted = Time.now.to_i
      @time = Time.at(time_posted).strftime("%a %b %d, %Y %I:%M %p")

      @test_email = "test@#{SITE_CONFIG[:allowed_domains].first}"

      @post = mock_model(Post, {
                :id => 1,
                :email => @test_email,
                :name => "My Post",
                :time_posted => time_posted,
                :security_id => 9999
              })

      @poster_email = "poster@#{SITE_CONFIG[:allowed_domains].first}"
      @message = Message.new :message => "Test",
                             :post_id => 1,
                             :ip      => "localhost",
                             :email   => @poster_email

      @message.stub!(:email).and_return(@poster_email)
      @message.stub!(:message).and_return("Test")

      @post.stub!(:time).and_return(@time)
      @post.stub!(:security_id).and_return(9999)
      @post.stub!(:id).and_return(1)
      @post.stub!(:name).and_return("My Post")
      @mail = NotifierMailer.msg(@message, @post)
    end

    it "should render the subject email" do
      @mail.subject.should == "#{SITE_CONFIG[:site_name]} - #{@poster_email} response: My Post"
    end

    it "should reply to the sender" do
      Rails.env.stub!(:production?).and_return(true)
      NotifierMailer.msg(@message, @post).reply_to.should include(@poster_email)
    end

    it "should reply to 'nhphong1406@gmail.com' in staging and development environments" do
      [:staging?, :development?].each do |env|
        Rails.env.stub!(env).and_return(true)
        NotifierMailer.msg(@message, @post).reply_to.should include('nhphong1406@gmail.com')
      end
    end

    it "should send to the recipient" do
      Rails.env.stub!(:production?).and_return(true)
      NotifierMailer.msg(@message, @post).to.should include(@test_email)
    end

    it "should send to 'nhphong1406@gmail.com' in staging and development environments" do
      [:staging?, :development?].each do |env|
        Rails.env.stub!(env).and_return(true)
        NotifierMailer.msg(@message, @post).to.should include('nhphong1406@gmail.com')
      end
    end

    it "should bcc to #{SITE_CONFIG[:bbc_email]} in production" do
      Rails.env.stub!(:production?).and_return(true)
      NotifierMailer.msg(@message, @post).bcc.should include(SITE_CONFIG[:bcc_email])
    end

    it "should render the sender's address" do
      @mail.from.should include(SITE_CONFIG[:short_noreply_email])
    end

    it "should show message of the poster in body" do
      @mail.body.encoded.should match("Test")
    end

    it "should show email of the poster in body" do
      @mail.body.encoded.should match("From: #{@poster_email}")
    end

    it "should show post name in body" do
      @mail.body.encoded.should match("My Post")
    end

    it "should show posted time in body" do
      @mail.body.encoded.should match(@time)
    end

    it "should show delete post url in body" do
      @mail.body.encoded.should match(delete_post_url(1, 9999))
    end

    it "should show #{SITE_CONFIG[:contact_email]} email" do
      @mail.body.encoded.should match(SITE_CONFIG[:contact_email])
    end
  end
end

