require 'spec_helper'

describe PagesController do
  render_views

  before(:each) do
    @base_title = ApplicationHelper::BASE_TITLE
  end

  describe "GET 'home'" do

    describe "links" do

      it "should have two links to all posts (search) page" do
        get 'home'
        response.should have_selector('a', :href => posts_path, :count => 3)
        response.should have_selector('a', :content => "recent posts", :href => posts_path, :count => 1)
        response.should have_selector('a', :content => Post.count_this_week.to_s, :href => posts_path, :count => 1)
      end

      it "should have a 'post to classifieds' link" do
        get 'home'
        response.should have_selector('a', :content => "post to classifieds",
                                           :href => new_post_path)
      end

      it "should have a 'by job post' link" do
        get 'home'
        response.should have_selector('a', :content => "buy job post", :href => "/jobs")
      end

      it "should have breadcrumbs" do
        breadcrumb_regex = /<nav>\s*<a href="\/">#{SITE_CONFIG[:site_name]}<\/a> &raquo;\s*<a href="\/">#{SITE_CONFIG[:university]}<\/a>\s*<\/nav>/

        get 'home'
        response.body.should match breadcrumb_regex
      end

      it "should show 50 recent posts" do
        get 'home'
        Post.active.recent.limit(50).each do |post|
          response.should have_selector("a", :content => post[:name], :href => post_path(post))
        end
      end

      it "should have overview links" do
        get 'home'

        CATEGORIES_ORDER.each do |c|
          name = CATEGORIES[c][:short_name]
          response.should have_selector( 'a', :content => name, :href => posts_path(:cat => c) )
        end
      end

      it "should have two links for each category " do
        get 'home'

        CATEGORIES_ORDER.each do |c|
          name = CATEGORIES[c][:short_name]
          response.should have_selector( 'a', :content => name, :href => posts_path(:cat => c), :count => 2 )
        end
      end

      it "should have links to each subcategory" do
        get 'home'

        subcategories = SUBCATEGORIES

        subs = SUBCATEGORIES

        subs.each_key {|id|
          if subs[id][:name_home].nil?
            name = subs[id][:name]
          else
            name = subs[id][:name_home]
          end
          response.should have_selector( 'a', :href => posts_path(:sub => id),
                                              :content => name,
                                              :count => 1 )
        }
      end
    end

    describe "section html" do
      it "should have the right title" do
        get 'home'
        response.should have_selector("title", :content => @base_title)
      end

      it "should have a non-blank body" do
        get 'home'
        response.body.should_not =~ /<body>\s*<\/body>/
      end

      it "should have a place for Google Ad" do
        get "home"
        response.should have_selector('td[class="ad"]')
      end

      it "should have an 'overview' section" do
        get 'home'
        response.should have_selector( 'div[class="overview round"]>h2', :content => "overview" )
      end

      it "should have 'recent posts' section title" do
        get 'home'
        response.should have_selector('h3', :content => "recent posts")
      end
    end

    it "should be successful" do
      get 'home'
      response.should be_success
    end
  end

  describe "GET 'about'" do
    it "should be successful" do
      get 'about'
      response.should be_success
    end

    it "should have the right title" do
      get 'about'
      response.should have_selector("title", :content => "#{@base_title} | About")
    end
  end

  describe "GET 'contact'" do
    it "should be successful" do
      get 'contact'
      response.should be_success
    end

    it "should have the right title" do
      get 'contact'
      response.should have_selector("title", :content => "#{@base_title} | Contact")
    end
  end

  describe "GET 'privacy'" do
    it "should be successful" do
      get 'privacy'
      response.should be_success
    end

    it "should have the right title" do
      get 'privacy'
      response.should have_selector("title",
                                    :content => "#{@base_title} | Privacy")
    end
  end

  describe "GET 'terms'" do
    it "should be successful" do
      get 'terms'
      response.should be_success
    end

    it "should have the right title" do
      get 'terms'
      response.should have_selector("title",
                                    :content => "#{@base_title} | Terms")
    end
  end

  describe "GET 'help'" do
    it "should be successful" do
      get 'help'
      response.should be_success
    end

    it "should have the right title" do
      get 'help'
      response.should have_selector("title",
                                    :content => "#{@base_title} | Help")
    end
  end
end

private

def nameRevisions( name )
  case name
     when "have housing" then "housing"
     when "off campus jobs" then "jobs"
     else name
  end
end

