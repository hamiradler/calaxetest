# encoding: utf-8
require 'spec_helper'

describe PostsController do
  render_views

# ====================================
# "GET 'index'"
# ====================================

  describe "GET 'index'" do

    before(:each) do
      loadPosts 20
    end

    it "should be successful" do
      get :index
      response.should be_success
    end

    it "should have the right css tag and header for each category search" do
      loadPosts 200

      cats = CATEGORIES

      cats.each_key do |id|
        get :index, :cat => id

        response.should have_selector("div[class='posts #{cats[id][:css]}']")
        response.should have_selector("div", :content => cats[id][:short_name])
      end
    end

    it "should have the right css tag and header for each subcategory search" do
      loadPosts 600
      subs = SUBCATEGORIES

      subs.each_key do |id|
        get :index, :sub => id
        cat = CATEGORIES[ subs[id][:category_id] ]
        response.should have_selector("div[class='posts #{cat[:css]}']") unless cat.nil?
        response.should have_selector("div", :content => subs[id][:name])
      end
    end

    it "should list the first post date at the top of the listings" do
      get :index
      response.should have_selector("ul>li", :content => @posts.first.short_date)
    end

    it "should list the first post title in the listing" do
      get :index
      response.should have_selector("li", :content => @posts[0][:name])
    end

    it "should have an element for each post on the first page (100 posts)" do
      get :index
      @posts[0..99].each do |post|
        response.should have_selector("li", :content => post[:name])
        response.should have_selector("a", :href => "/posts/#{post.id}" )
      end
    end

    it "should show all new date dividers (header) when posts with new dates are displayed" do
      get :index
      response.should have_selector('li[class="date"]', :count => dayCount)
    end

    it "should have an element for each post" do
      get :index
      @posts[0..2].each do |post|
        response.should have_selector("li", :content => post.name)
      end
    end

    it "should paginate posts" do
      loadPosts 101

      get :index
      response.should have_selector("div.pagination")
      response.should have_selector("span.disabled", :content => "Previous")
      response.should have_selector("a", :href => "/posts?page=2",
                                         :content => "Next")
    end

    it "should allow text searches" do
      get :index, :q => "niece"
      response.should be_success
    end

    it "should find the right posts based on a text search query" do
      titles = ["my bike", "isn't my niece the cutest ever!!!", "my house"]
      attrs = { :email => "gwientjes@gmail.com", :body => "Test" }

      400.times do
        name = titles.sample
        attrs.merge! :name => titles.sample
        @posts << Factory( :post, attrs )
      end

      get :index, :q => "cutest"

      response.should have_selector("a", :content => "cutest", :count => 100)
    end

    it "should fail gracefully if no posts found in a search" do
      get :index, :q => "no_posts_will_be_found_with_this_search_query"
      response.should be_success
      response.should have_selector("h1",
                                        :content => "I couldn't find any posts")
    end

    it "should have breadcrumbs for category" do
      breadcrumbsCategory :index
    end

    it "should have breadcrumbs for subcategory" do
      breadcrumbsSubcategory :index
    end

    it "should have a photo icon if post has a photo" do
      # remember before each is added 20 posts with photos
      noImages = { :email => "gwientjes@gmail.com" }
      2.times { Factory( :post, noImages ) }
      2.times { Factory( :post, noImages.merge(:image_source1 => File.new("#{Rails.root}/public/images/Camera.gif")) ) }

      get :index, :q => "cutest"
      response.should have_selector("img", :alt => "Camera")
    end

    it "should have a photo icon if post has a photo in any field" do
      noImages = { :email => "gwientjes@gmail.com" }
      2.times { Factory( :post, noImages ) }
      2.times { Factory( :post, noImages.merge(:image_source1 => File.new("#{Rails.root}/public/uploads/raw/asha.jpeg")) ) }
      2.times { Factory( :post, noImages.merge(:image_source2 => File.new("#{Rails.root}/public/uploads/raw/asha.jpeg")) ) }
      2.times { Factory( :post, noImages.merge(:image_source3 => File.new("#{Rails.root}/public/uploads/raw/asha.jpeg")) ) }
      2.times { Factory( :post, noImages.merge(:image_source4 => File.new("#{Rails.root}/public/uploads/raw/asha.jpeg")) ) }

      get :index, :q => "cutest"
      response.should have_selector("img", :alt => "Camera")
    end

  end

  # ====================================
  # "GET 'show'"
  # ====================================

  describe "GET 'show'" do
    before(:each) do
      @post = Factory(:post, { :name => "Test", :body => "Test", :email => "gwientjes@gmail.com"})
    end

    it "should be successful" do
      get 'show', :id => @post
      response.should be_success
    end

    it "should find the right post" do
      get :show, :id => @post
      assigns(:post).should == @post  # assigns gets the instanciated post variable within the controller.  This variable should be the same as the original Factory post.
    end

    it "should display the right css post category" do
      get :show, :id => @post
      response.should have_selector("div[class='post #{@post.css}']")
    end


    it "should display photo" do
      get :show, :id => @post
      response.should have_selector("div[class='photos']>img")
    end

    it "should have the right page title" do
      get :show, :id => @post
      response.should have_selector('title', :content => @post.name)
    end

    it "should have the post title" do
      get :show, :id => @post
      response.should have_selector("h1", :content => @post.name)
    end

    it "should have the post body" do
      get :show, :id => @post
        # String of the post body should be displayed on the post show page.
        # I test just 6 characters here, because my use of
        # TextHelper simple_format causes changes in the
        # html, e.g. <p></p> insertions
      response.body.should match( @post.body[0..8] )
    end

    it "should have breadcrumbs" do
      get 'show', :id => @post

      crumb = "#{SITE_CONFIG[:site_name]} » #{SITE_CONFIG[:university]} » #{@post.category} » #{@post.subcategory}"
      response.should contain( crumb )

      response.should have_selector('a', :content => @post.category,
                          :href => posts_path( :cat => @post.category_id ))
      response.should have_selector('a', :content => @post.subcategory,
                          :href => posts_path( :sub => @post.subcategory_id ))

    end
  end

  describe "GET 'new'" do

    it "should be successful" do
      get 'new'
      response.should be_success
    end

    it "should have the right title" do
      get 'new'
      response.should have_selector('title', :content => "Post")
    end

    it "should have a request to select a post category" do
      get 'new'
      response.should have_selector('h1',
                          :content => "What type of post is this?")

    end

    it "should display the category option for 'for sale'" do
      get 'new'
      response.should have_selector('li>a', :content => "for sale / wanted")
    end


    it "should have a request to select a post subcategory" do
      get 'new', :cat => 5
      response.should have_selector('h1',
                          :content => "Please choose a category")
    end

    it "should have a non-empty @subcategories after category chosen" do
      get 'new', :cat => 5
      assigns[:subcategories].should_not be_empty
    end

    it "should display the subcategory options" do
      get 'new', :cat => 5
      response.should have_selector('li')
      response.should have_selector("div[class='subcategories']>ul>li")
    end

    it "should have breadcrumbs for category" do
      breadcrumbsCategory :new
    end

    it "should have breadcrumbs for subcategory" do
      breadcrumbsSubcategory :new
    end

  end

  describe "POST 'create'" do
    describe "failure" do
      before(:each) do
        session[:post] = {}
        @attr = {     :name=> "", :body => "",
                      :email=>"",
                      :category_id => 5, :subcategory_id => 4         }
      end

      it "should redirect to the new page if session[:post] is nil" do
        session[:post] = nil
        post :create, :post => @attr
        response.should redirect_to(new_post_path)
      end

      it "should have the right title" do
        post :create, :post => @attr
        response.should have_selector('title', :content => "Post")
      end

      it "should render the new page" do
        post :create, :post => @attr
        response.should render_template('new')
      end

      it "should not create a post" do
        lambda do
          post :create, :post => @attr, :preview => :preview
        end.should_not change(Post, :count)
      end
    end

    describe "success" do
      before(:each) do
          session[:post] = {}
          @attr = { :name => "My Bike", :body => "My bike for sale",
                    :email => "gwientjes@gmail.com",
                    :category_id => 5, :subcategory_id => 4 }
      end

      it "should create a post" do
        lambda do
          post :create, :post => @attr, :preview => :preview
        end.should change(Post, :count).by(1)
      end

      it "should render post preview page" do
        post :create, :post => @attr, :preview => :preview
        response.should render_template('preview')
      end

      it "should show Message Poster form" do
        post :create, :post => @attr, :preview => :preview
        response.body.should_not match /Message Poster/
      end

      it "should show continue button" do
        post :create, :post => @attr, :preview => :preview
        response.should have_selector('div[class="actions_preview_btn"]>input' )
      end

      it "should have the right title" do
        post :create, :post => @attr, :preview => :preview
        response.should have_selector('title', :content => "Publish your post")
      end
    end
  end

  describe "GET 'instructions'" do
    before :each do
      get :instructions
    end

    it "should be successful" do
      response.should be_success
    end

    it 'should have start another post link' do
      response.should have_selector("a", :href => new_post_path, :content => "Start another post.")
    end

    it "should have root url" do
      response.should have_selector("a", :href => root_path, :content => "Return to #{SITE_CONFIG[:site_name]}.")
    end
  end

  describe "GET 'legacy_publish'" do
    it "should redirect to publish action" do
      get :legacy_publish, :id => 1, :security_id => 9999
      response.should redirect_to(:action => :publish, :id => 1, :security_id => 9999)
    end
  end

  describe "GET 'legacy_delete'" do
    it "should redirect to delete action" do
      get :legacy_delete, :id => 1, :security_id => 9999
      response.should redirect_to(:action => :delete, :id => 1, :security_id => 9999)
    end
  end

  describe "GET 'publish'" do
    describe "success" do
      before(:each) do
        @post = Post.create :name => "bike",
                            :body => "My bike for sale.",
                            :email => "gwientjes@gmail.com",
                            :category_id => 5,
                            :subcategory_id => 4

        Post.should_receive(:find_by_id_and_security_id).with(@post.id, @post.security_id).and_return(@post)
        @post.should_receive(:republish)
        get :publish, :id => @post.id, :security_id => @post.security_id
      end

      it "should be successful" do
        response.code.should eq("200")
      end

      it "should have 'view your post' link" do
        response.should have_selector(:a, :href => post_path(@post), :content => "View your post")
      end

      it "should have 'delete your post' link" do
        response.should have_selector(:a, :href => delete_post_path(@post), :content => "Delete your post")
      end

      it "should have '#{SITE_CONFIG[:site_name]}' link" do
        response.should have_selector(:a, :href => root_path, :content => SITE_CONFIG[:site_name])
      end
    end

    describe "failure" do
      it "should be failed" do
        lambda {
          get :publish, :id => 1, :security_id => 9999
        }.should raise_error(NoMethodError)
      end
    end
  end

  describe "GET 'delete'" do
    describe "success" do
      before(:each) do
        @post = Post.create :name  => "bike",
                            :body  => "My bike for sale.",
                            :email => "gwientjes@gmail.com",
                            :category_id => 5,
                            :subcategory_id => 4
        Post.should_receive(:find_by_id_and_security_id).with(@post.id, @post.security_id).and_return(@post)

        @post.should_receive(:unpublish)

        get :delete, :id => @post.id, :security_id => @post.security_id
      end

      it "should be successful" do
        response.code.should eq("200")
      end

      it "should have 'Republish your post' link" do
        response.should have_selector(:a, :href => publish_post_path(@post), :content => "Republish your post")
      end

      it "should have '#{SITE_CONFIG[:site_name]}' link" do
        response.should have_selector(:a, :href => root_path, :content => SITE_CONFIG[:site_name])
      end
    end

    describe "failure" do
      it "should be failed" do
        lambda {
          get :delete, :id => 1, :security_id => 9999
        }.should raise_error(NoMethodError)
      end
    end
  end
end

def breadcrumbsSubcategory page
  subs = SUBCATEGORIES

  subs.each_key do |id|
    get page, :sub => id

    c = CATEGORIES[ subs[id][:category_id] ]

    crumb = "#{SITE_CONFIG[:site_name]} » #{SITE_CONFIG[:university]} » #{c[:short_name]} » #{subs[id][:name]}"
    response.should contain( crumb )

    response.should have_selector('a', :content => c[:short_name],
                        :href => posts_path( :cat => subs[id][:category_id] ))
    response.should have_selector('a', :content => subs[id][:name],
                        :href => posts_path( :sub => id ))
  end
end

def breadcrumbsCategory page

  cats = CATEGORIES
  # @categories.delete(6)

  cats.each_key do |id|
    get page, :cat => id
    crumb = "#{SITE_CONFIG[:site_name]} » #{SITE_CONFIG[:university]} » #{cats[id][:short_name]}"
    response.should contain( crumb )
    response.should have_selector('a', :content => cats[id][:short_name],
                                      :href => posts_path( :cat => id ))
  end
end

# Count days within the posts
def dayCount
  count = 0
  @posts[0..99].each do |post|
    count += 1 if newDay?( post )
  end
  # p "count"
  # p count
  count
end


# Date string returned based on time in seconds
def date( time )
  Time.at(time).strftime("%a, %b %d")
end

# New Day? Return a boolean whether post is a new day compared to last post
# tested. Function used in the 'posts' page (search results) for dividing
# posts into groups by day with the date displayed as a divider.
def newDay?( post )
  if @lastTime.nil?
    @lastTime = post[:time_posted]    # this is the first post tested
    true
  elsif sameDay?( @lastTime, post[:time_posted] )
    false
  else
    @lastTime = post[:time_posted]
    true
  end
end

# Compares two times and returns boolean whether they are the same day.
def sameDay?( time1, time2 )
  date( time1 ) == date( time2 )
end

