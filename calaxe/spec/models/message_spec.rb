require "spec_helper"

describe Message do
  describe "Virtual Attributes / Instance methods" do
    before :each do
      @msg = Message.new
    end

    it "should have a post association" do
      @msg.should respond_to(:post)
    end

    it "should have a send_to_poster method" do
      @msg.should respond_to(:send_to_poster)
    end
  end

  describe "#send_to_poster" do
    before :each do
      @msg = Message.new
    end

    it "should receive a post as a parameter" do
      mailer = mock("Mailer")
      post = Post.new
      NotifierMailer.should_receive(:msg).with(@msg, post).and_return(mailer)
      mailer.should_receive(:deliver)
      @msg.send_to_poster(post)
    end
  end
end

