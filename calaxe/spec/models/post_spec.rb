require 'spec_helper'

describe Post do

  before(:each) do
    @attr = {     :name=> "bike", :body => "My bike for sale.",
                  #:email=>"wientjes@stanford.edu",
                  :email => 'gwientjes@gmail.com',
                  :category_id => 5, :subcategory_id => 4         }
  end

  describe "Required attributes" do

    it "should require a post title" do
      no_title_post = Post.new( @attr.merge( :name => "" ) )
      no_title_post.should_not be_valid
    end

    it "should require an email address" do
      no_email_post = Post.new( @attr.merge( :email => "" ) )
      no_email_post.should_not be_valid
    end

    it "should require body text in the post" do
      no_body_post = Post.new( @attr.merge( :body => "" ) )
      no_body_post.should_not be_valid
    end

    it "should require a category_id" do
      no_category_id_post = Post.new( @attr.merge( :category_id => "" ) )
      no_category_id_post.should_not be_valid
    end

    it "should require a subcategory_id" do
      no_subcategory_id_post = Post.new( @attr.merge( :subcategory_id => "" ) )
      no_subcategory_id_post.should_not be_valid
    end
  end

  describe "Invalid format" do

    it "should reject titles that are too long" do
      long_title = "a" * 72
      long_title_post = Post.new( @attr.merge( :name => long_title))
      long_title_post.should_not be_valid
    end

    it "should reject body text that is too long" do
      long_body = "a" * 32001
      long_body_post = Post.new( @attr.merge( :body => long_body ))
      long_body_post.should_not be_valid
    end

    it "should reject invalid email addresses" do
      addresses = %w[user@foo,com user_at_foo.org example.user@foo. john@stanfordalumni,org john@gmail.com bill_james@hotmail.com wientjes@stanford.edu test@berkeley.com test@1alumni.berkeley.com]
      addresses.each do |address|
        invalid_email_post = Post.new( @attr.merge( :email => address ))
        invalid_email_post.should_not be_valid
      end
    end
  end

  # stanford.edu stanfordgsb.org stanfordalumni.org
  # Validate university emails
  it "should accept valid email addresses" do
    addresses = SITE_CONFIG[:allowed_domains].map { |d| "test@#{d}" }
    addresses << "gwientjes@gmail.com"
    addresses << "nhphong1406@gmail.com"

    addresses.each do |address|
      valid_email_post = Post.new( @attr.merge( :email => address ))
      valid_email_post.should be_valid
    end
  end

  describe "Virtual attributes / Instance methods" do
    before(:each) do
      @post = Post.new
    end

    it "should have an activate method" do
      @post.should respond_to(:activate)
    end

    it "should have an active? method" do
      @post.should respond_to(:active?)
    end

    it "should have a category attribute" do
      @post.should respond_to(:category)
    end

    it "should have a date attribute" do
      @post.should respond_to(:date)
    end

    it "should have a deleted? method" do
      @post.should respond_to(:deleted?)
    end

    it "should have a deliver_activate_mail method" do
      @post.should respond_to(:deliver_activate_mail)
    end

    it "should have a full_time method" do
      @post.should respond_to(:full_time)
    end

    it "should have a has_photo? method" do
      @post.should respond_to(:has_photo?)
    end

    it "should have a new_day? method" do
      @post.should respond_to(:new_day?)
    end

    it "should have a photo method -- gets a sample photo" do
      @post.should respond_to(:photo)
    end

    it "should have a photos method" do
      @post.should respond_to(:photos)
    end

    it "should have a short_date attribute" do
      @post.should respond_to(:short_date)
    end

    it "should have a subcategory attribute" do
      @post.should respond_to(:subcategory)
    end

    it "should have a time attribute" do
      @post.should respond_to(:time)
    end

    it "should have an unpublish method" do
      @post.should respond_to(:unpublish)
    end

    it "should have an unpublished? attribute" do
      @post.should respond_to(:unpublished?)
    end

    it "should have a messages association" do
      @post.should respond_to(:messages)
    end
  end

  describe "Class methods" do
    it "should have a category_activity_metrics method" do
      Post.should respond_to(:category_activity_metrics)
    end

    it "should have a count_this_week method" do
      Post.should respond_to(:count_this_week)
    end

    it "should have a create_with_id method" do
      Post.should respond_to(:create_with_id)
    end

    it "should have a find_recent_photos method" do
      Post.should respond_to(:find_recent_photos)
    end

    it "should have a posts method" do
      Post.should respond_to(:posts)
    end

     it "should have a subcategories method" do
      Post.should respond_to(:subcategories)
    end

    it "should have a has_photo method" do
      Post.should respond_to(:has_photo)
    end

    it "should have an active method" do
      Post.should respond_to(:active)
    end

    it "should have a recent method" do
      Post.should respond_to(:recent)
    end

    it "should have a this_week method" do
      Post.should respond_to(:this_week)
    end

    it "should have a category method" do
      Post.should respond_to(:category)
    end

    it "should have a subcategory method" do
      Post.should respond_to(:subcategory)
    end

    it "should have a query method" do
      Post.should respond_to(:query)
    end

  end

  describe "Filled in data" do

    it "should have a non-blank css attribute after finding this post" do
      # post = Post.new
      post = Post.create!(@attr)
      post = Post.find post
      post.css.should_not be_blank
    end

    it "should have a non-blank time attribute after finding this post" do
      # post = Post.new
      post = Post.create!(@attr)
      post = Post.find post
      post.time.should_not be_blank
    end

    it "should return return the expected subcategories count" do
      sub = Post.subcategories 5
      sub.count.should == 16
    end

    it "should get photos" do
      postsWithPhotos = Post.find_recent_photos
      postsWithPhotos.each do |post|
        post.photos.length.should > 0
      end
    end
  end

  it "should create a new instance given a valid attribute" do
    Post.create!( @attr )
  end

  it "should have Category and Subcategory data in Post model" do
    CATEGORIES[5][:name].should == "for sale / wanted"
    SUBCATEGORIES[7][:name].should == "cars"
    SUBCATEGORIES[7][:category_id].should == 5
  end

  it "should upload a photo" do
    post = Post.new :image_source1 => File.new( Rails.root + 'spec/fixtures/images/logo.gif')
    post.image_source1.url.include?("missing").should be_false
    post.image_source2.url.include?("missing").should be_true
  end

  it "should upload 4 photos" do
    post = Post.new :image_source1 => File.new( Rails.root + 'spec/fixtures/images/logo.gif')
    post.image_source1.url.include?("missing").should be_false
    post.image_source2.url.include?("missing").should be_true

    post = Post.new :image_source2 => File.new( Rails.root + 'spec/fixtures/images/logo.gif')
    post.image_source2.url.include?("missing").should be_false
    post.image_source1.url.include?("missing").should be_true

    post = Post.new :image_source3 => File.new( Rails.root + 'spec/fixtures/images/logo.gif')
    post.image_source3.url.include?("missing").should be_false
    post.image_source1.url.include?("missing").should be_true

    post = Post.new :image_source4 => File.new( Rails.root + 'spec/fixtures/images/logo.gif')
    post.image_source4.url.include?("missing").should be_false
    post.image_source1.url.include?("missing").should be_true
  end


  describe "Search" do
    it "should get category activity metrics" do
      loadPosts 200

      activity = Post.category_activity_metrics

      CATEGORIES.each_key do |id|
        activity[id][:recent_time] == Post.category(id).recent.limit(1).first.full_time
        activity[id][:count] == Post.category(id).count_this_week
      end
    end

    it "should get recent posts with photos" do
      loadPosts 100

      posts = Post.find_recent_photos
      posts.length.should == 4

      posts.each do |post|
        post.has_photo?.should be_true
        post.active?.should be_true
      end
    end

    it "should get posts with photos" do
      loadPosts 100

      posts = Post.has_photo
      posts.each do |post|
        post.has_photo?.should be_true
      end
    end

    it "should limit posts returned with limit method" do
      loadPosts 100

      posts = Post.limit(4)
      posts.length.should == 4
    end

    it "should get the posts this week" do
      posts = Post.this_week
      posts.length == Post.active.where( ["time_posted > ?",
                                          Time.now.to_i - 7.days])
    end

    it "should count the posts this week" do
      count = Post.count_this_week
      count == Post.this_week.active.length
    end
  end

  describe "Post Function" do
    before :each do
      @new_post = Post.create :name => "bike",
                              :body => "My bike for sale.",
                              :email =>"wientjes@stanford.edu",
                              :category_id => 5,
                              :subcategory_id => 4
    end
    it "should create an unpublished post after student posts a new post" do
      @new_post.unpublished?.should be_true
    end

    it "should activate an unpublished post" do
      @new_post.should_receive(:unpublished?).and_return(true)
      @new_post.should_receive(:update_attributes).with(:status => Post::STATUS_ACTIVE).and_return(true)
      @new_post.activate.should be_true
    end

    it "should activate a 'deleted' post" do
      @new_post.status = Post::STATUS_DELETED
      @new_post.should_receive(:unpublished?).and_return(false)
      @new_post.should_receive(:deleted?).and_return(true)
      @new_post.should_receive(:update_attributes).with(:status => Post::STATUS_ACTIVE).and_return(true)
      @new_post.activate.should be_true
    end

    it "should not activate a 'active' post" do
      @new_post.status = Post::STATUS_ACTIVE
      @new_post.should_receive(:unpublished?).and_return(false)
      @new_post.should_receive(:deleted?).and_return(false)
      @new_post.should_not_receive(:update_attributes).with(:status => Post::STATUS_ACTIVE).and_return(true)
      @new_post.activate.should be_nil
    end

    it "should unpubish an 'active' post" do
      @new_post.status = Post::STATUS_ACTIVE
      @new_post.should_receive(:active?).and_return(true)
      @new_post.should_receive(:update_attributes).with(:status => Post::STATUS_DELETED).and_return(true)
      @new_post.unpublish.should be_true
    end

    it "should not unpubish a 'deleted' post" do
      @new_post.status = Post::STATUS_DELETED
      @new_post.should_receive(:active?).and_return(false)
      @new_post.should_not_receive(:update_attributes).with(:status => Post::STATUS_DELETED).and_return(true)
      @new_post.unpublish.should be_nil
    end

    it "should not unpubish an 'unpublished' post" do
      @new_post.status = Post::STATUS_UNPUBLISHED
      @new_post.should_receive(:active?).and_return(false)
      @new_post.should_not_receive(:update_attributes).with(:status => Post::STATUS_DELETED).and_return(true)
      @new_post.unpublish.should be_nil
    end

    it "should deliver activate mail with a post argument" do
      mailer = mock("Mailer")
      NotifierMailer.should_receive(:activate).with(@new_post).and_return(mailer)
      mailer.should_receive(:deliver)
      @new_post.deliver_activate_mail
    end
  end

  # describe "Search on posts index" do
  #   it "should have posts searchable by category_id" do
  #     200.times do
  #       # @posts << Factory(:post, :category_id => Factory.next(:category_id))
  #       @posts << Factory(:post, :category_id => (1..8).to_a.sample)
  #     end
  #
  #     @posts << Factory(:post, :category_id => 5)
  #
  #     params = { :cat => 5 }
  #     # Post.posts( params ).count.should > 0
  #     logger.debug "Post.posts( params ).count"
  #     p Post.posts( params ).count
  #     # Post.category(5).count == (Post.posts params).count
  #   end
  #
  #   it "should have posts searchable by subcategory_id"
  #   it "should have posts result counts corresponding between category and subcategory"
  # end

  # it "should have a non-blank ip field in a post" do
  #   @post = Post.create!(@attr)
  #   @post = Post.find @post
  #   @post.ip.should_not be_blank
  # end
end

