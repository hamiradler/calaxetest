FactoryGirl.define do
  factory :post do
    name                       "isn't my niece the cutest ever!!!"
    body                       "my niece!\n\n\n\n\nJe t'aime."
    email                      "wientjes@stanford.edu"
    category_id                8
    subcategory_id             130
    image_source1_file_name    "1193656956a.jpeg"
    image_source2_file_name    "NULL"
    image_source3_file_name    "NULL"
    image_source4_file_name    "NULL"
    time_posted                "134545645"
    status                     1
  end
end

