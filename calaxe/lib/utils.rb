module ActiveSupport
  class TimeWithZone
    def zone=(new_zone = ::Time.zone)
      # Reinitialize with the new zone and the local time
      initialize(nil, ::Time.__send__(:get_zone, new_zone), time)
    end
  end
end

class Time
  def self.now_in_time_zone(zone=SITE_CONFIG[:time_zone])
    now.in_time_zone(zone)
  end
end
