def vps
  fetch :vps
end

def stage
  fetch(:stage).to_s
end

def rails_env
  fetch :rails_env
end

def user
  fetch :user, 'deploy'
end

def application
  fetch :application, 'berkelist'
end

def domain
  fetch :domain
end

def deploy_to
  fetch :deploy_to, "/home/#{user}/#{application}"
end

def app_root
  (current_path || File.join(deploy_to, 'current')).to_s
end

def public_path
  File.join app_root, 'public'
end

def app_config
  "#{application}.yml"
end

def app_s3
  case application
  when 'huskylist'
    'huskielist'
  when 'berkelist'
    'calaxe'
  when 'illinian'
    'uofilist'
  else
    application
  end
end

def app_database
  application
end

def nginx_script_path
  '/etc/init.d/nginx'
end

def puma
  "puma-#{application}"
end

def puma_nginx_config
  "nginx-#{puma}"
end

def puma_nginx_config_path
  "/etc/nginx/sites-enabled/#{puma_nginx_config}"
end

def puma_socket_path
  File.join(app_root, 'tmp', 'sockets', 'puma.sock')
end

def puma_script
  puma
end

def puma_script_path
  "/etc/init.d/#{puma}"
end

def unicorn
  "unicorn-#{application}"
end

def unicorn_nginx_config
  "nginx-#{unicorn}"
end

def unicorn_nginx_config_path
  "/etc/nginx/sites-enabled/#{unicorn_nginx_config}"
end

def unicorn_socket_path
  File.join(app_root, 'tmp', 'sockets', 'unicorn.sock')
end

def unicorn_script
  unicorn
end

def unicorn_script_path
  "/etc/init.d/#{unicorn}"
end

def parse_cron
  "parse-#{application}"
end

def parse_cron_dir
  File.join('~', 'cron', 'parse')
end

def parse_cron_path
  File.join parse_cron_dir, application
end

def threetaps_cron
  "3taps-#{application}"
end

def threetaps_cron_dir
  File.join('~', 'cron', '3taps')
end

def threetaps_cron_path
  File.join threetaps_cron_dir, application
end

class DeployConfig
  DOMAINS = {
    'kthkap' => 'se'
  }

  APPLICATIONS = {
    # trojy.com
    1 => %w[ beaverwall berkelist bigredlist bluejayslist bobcatlist boilist brulist buckeyeslist cmtart cuwal hoklist huskylist ],

    # BuckEyesList.com
    2 => %w[ huwall illinian kingtrit pupost sundevlist trojy txanm uflist umwall utxpost wislist yelljack kthkap ],

    # gtownlist.com
    3 => %w[ aggiepost bearspost bigjaylist bisonlist buflist caslist cockylist cometslist coyolist dartslist duklist goldbluelist grizpost gtownlist henlist ],

    # tigerswall.com
    4 => %w[ huskerlist iscyc lclist lobolist miamislist msulist odpost okupost owlswall quakerslist rebslist rowdylist tideslist tigerswall tulpost ],

    # wildcatlist.com
    5 => %w[ uaalist uawall uchlist uhpost uidpost uminlist umpost uteslist uvwall vandylist vikslist wildcatlist wyolist yallist ],

    # sydpost.com
    6 => %w[ aucklist birmlist calgalist camblist curtinlist manclist mellist montlist ottalist oxflist qutechlist sydpost ubritwall ],

    7 => %w[ gtechlist lavishlevity nascentnexus ],

    8 => %w[ cornellpost ]
  }

  SERVERS = [
    [ '1', 'trojy',        '198.199.110.148' ], # trojy.com
    [ '2', 'buckeyeslist', '107.170.239.155' ], # BuckEyesList.com
    [ '3', 'gtownlist',    '192.241.230.21'  ], # gtownlist.com
    [ '4', 'tigerswall',   '162.243.139.191' ], # tigerswall.com
    [ '5', 'wildcatlist',  '162.243.140.166' ], # wildcatlist.com
    [ '6', 'sydpost',      '198.199.116.190' ], # sydpost.com (international university)
    [ '7', 'gtechlist',    '162.243.128.81'  ], # gtechlist.com (host 5 clone sites)
    [ '8', 'cornellpost',  '107.170.175.28'  ]  # test by john
  ]

  SERVER_BY_GROUP = SERVERS.inject({}) do |hash, (idx, name, ip)|
    num        = idx.to_i
    hash[idx]  = ip
    hash[num]  = ip
    hash[name] = ip
    hash[ip]   = ip
    hash
  end

  SERVER_BY_APP = APPLICATIONS.inject({}) do |hash, (group, apps)|
    apps.each { |app| hash.update app => SERVER_BY_GROUP[group] }
    hash
  end

  GROUPS = SERVERS.map { |s| s[0].to_i }

  attr_reader :server, :group, :application, :anchor

  def initialize
    @server      = nil
    @group       = ENV['GROUP']
    @application = ENV['APP'] || applications_by_group[0] || fetch(:application, 'berkelist')
    @anchor      = ENV['ANCHOR'].to_s.dup.strip
  end

  def domain
    ext = DOMAINS[application] || 'com'
    "www.#{application}.#{ext} #{application}.#{ext}" # "www.gtechlist.com gtechlist.com"
  end

  def groups
    GROUPS
  end

  def applications
    Array( group ? APPLICATIONS[group.to_i] || application : application )
  end

  def databases
    applications
  end

  def websites
    applications.map { |app| "http://#{app}.#{DOMAINS[app] || 'com'}" }
  end

  def build_command(action, args = {})
    opts = args.map { |env, value| "#{env}=#{value}" }.join(' ')
    "bundle exec cap #{stage} #{action} #{opts}"
  end

  def open_program
    @open_program ||= if config.command_exist?('chromium-browser')
                        'chromium-browser %s >/dev/null 2>&1 &'
                      elsif config.command_exist?('google-chrome')
                        'google-chrome %s >/dev/null 2>&1 &'
                      elsif config.command_exist?('gnome-www-browser')
                        'gnome-www-browser %s >/dev/null 2>&1 &'
                      elsif config.command_exist?('x-www-browser')
                        'x-www-browser %s >/dev/null 2>&1 &'
                      elsif config.command_exist?('xdg-open')
                        'xdg-open %s'
                      elsif config.command_exist?("/Applications/Chromium.app/Contents/MacOS/Chromium")
                        "/Applications/Chromium.app/Contents/MacOS/Chromium %s >/dev/null 2>&1 &"
                      else
                        'open %s'
                      end
  end

  def parse_options!
    find_server
    validate_server!
  end

  def validate_group!
    unless SERVER_BY_GROUP[group]
      puts "ERROR: Unknown group. Please specify GROUP=<group>, with <group> is index or name"

      table = SERVERS.dup.unshift ['Index', 'Name', 'Server']
      print_table table

      exit 1
    end
  end

  def validate_anchor!
    if anchor.empty?
      puts "ERROR: Unknown anchor. Please specify ANCHOR=<anchor>"
      exit 1
    end
  end

  protected

  def command_exist?(command)
    output = `which #{command} 2>/dev/null`.chomp
    output.empty? ? nil : output
  end

  def applications_by_group
    Array( APPLICATIONS[group.to_i] )
  end

  def print_table(table)
    max = Array.new(table.first.length, 0)
    table.each_with_index do |row, ridx|
      row.each_with_index do |column, cidx|
        max[cidx] = column.length if max[cidx] < column.length
      end
    end
    max = max.map { |len| len + 2 }

    border = "+#{max.map { |len| '-' * len }.join('+')}+"
    table.each_with_index do |row, ridx|
      puts border
      if ridx == 0
        row = row.each_with_index.map { |column, idx| column.center( max[idx] ) }
      else
        row = row.each_with_index.map { |column, idx| " " + column.ljust( max[idx] - 1 ) }
      end
      puts "|#{row.join("|")}|"
    end
    puts border
  end

  def validate_server!
    unless server
      puts "* ERROR: Server is invalid"
      exit 1
    end
  end

  def find_server
    @server ||= begin
                  @server = SERVER_BY_GROUP[group]     if group
                  @server = SERVER_BY_APP[application] if !@server && application
                  @server
                end
  end
end

def config
  @config ||= DeployConfig.new
end
