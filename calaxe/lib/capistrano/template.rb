require 'erb'
require 'ostruct'

def template_dir
  'config/templates'
end

def template(from, to, options={})
  from = File.join template_dir, from
  erb  = File.open(from){ |f| f.read }
  erb  = ERB.new(erb).result(binding)
  erb  = StringIO.new erb
  to   = File.join(options[:remote_dir] || release_path, to)
  upload! erb, to
end
