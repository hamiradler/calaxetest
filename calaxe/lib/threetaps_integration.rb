require 'open-uri'
require 'threetaps_setup'

module ThreetapsIntegration
  FIELDS_FOR_SEARCH = {
    'Name'     => 'name',
    'ID'       => 'id',
    'Email'    => 'email',
    '3taps ID' => 'threetaps_id'
  }.to_a

  CATEGORIES_3TAPS = {
    'VVVV' => 5,
    'SSSS' => 5,
    'JJJJ' => 2,
    'RRRR' => 3
  }

  SUBCATEGORIES_3TAPS = {
    'VVVV' => 151,
    'SSSS' => 151,
    'JJJJ' => 158,
    'RRRR' => 60
  }

  FIELDS_3TAPS = {
    'VVVV' => :for_sale,
    'SSSS' => :for_sale,
    'JJJJ' => :jobs,
    'RRRR' => :housing
  }

  RETVALS = %w[
    id source category category_group external_id external_url
    heading body timestamp images annotations deleted
  ]

  def import_logger
    self.class.import_logger
  end

  def save_3taps_post(posting)
    category = MAPPINGS[ posting['category'] ]
    if category.blank?
      category_group = posting['category_group']
      category_id    = CATEGORIES_3TAPS[category_group]
      subcategory_id = SUBCATEGORIES_3TAPS[category_group]
    else
      category_id, subcategory_id = category.values_at(:category_id, :subcategory_id)
    end

    self.from_admin      = true
    self.name            = posting['heading']
    self.body            = posting['body']
    self.time_posted     = posting['timestamp'].to_i
    self.email           = posting['email']
    self.category_id     = category_id
    self.subcategory_id  = subcategory_id
    self.status          = posting['deleted'] ? Post::STATUS_DELETED : Post::STATUS_ACTIVE
    self.threetaps_id    = posting['id']
    self.external_id     = posting['external_id']
    self.external_url    = posting['external_url']

    images = posting['images'] || []
    if (self.new_record? || !self.has_photo?) && images.present?
      idx = 1
      images.each do |image|
        tried = 0
        import_logger.info ".. Downloading #{image.inspect}"
        begin
          self.send(:"image_source#{idx}=", open(image))
          self.send(:"image_source#{idx}_file_name=", File.basename(image))
          idx += 1
        rescue SocketError, OpenURI::HTTPError => ex
          import_logger.info ".. ERROR: #{ex.message}"
          self.send(:"image_source#{idx}=", nil)
          self.send(:"image_source#{idx}_file_name=", nil)
          tried += 1
          retry if tried < 4
        end
        break if idx > 4
      end
    end
    save!
  end

  def in_transaction
    begin
      self.class.transaction do
        yield(self) if block_given?
      end
    rescue Interrupt
      import_logger.info "\n.. Exiting from Post#in_transaction!"
      exit 1
    rescue StandardError => ex
      import_logger.info ".. ERROR: #{ex.message}"
      Error.generate_from_exception!(ex)
    end
  end

  module ClassMethods
    def import_logger
      @import_logger ||= ActiveSupport::BufferedLogger.new(Rails.env.development? ? STDOUT : 'log/3taps.log')
    end

    def threetaps_filters
      @threetaps_filters ||= YAML.load_file("#{Rails.root}/config/3taps.yml")
    end

    def location
      threetaps_filters['location']
    end

    def keywords
      threetaps_filters['keywords']
    end

    def default_options
      {
        :retvals        => RETVALS.join(','),
        :source         => 'CRAIG',
        :category_group => 'SSSS|JJJJ|RRRR|VVVV'
      }
    end

    def poll_options
      default_options.merge :location => location
    end

    def search_options
      default_options.merge :rpp => 100, :location => location
    end

    def keyword_regex
      return nil if keywords.blank?
      s = keywords.map{ |w| "(#{w})" }.join('|')
      /\b(#{s})\b/
    end

    def read_anchor
      begin
        anchor = File.read("#{Rails.root}/config/anchor.txt").try(:chomp)
        anchor.present? ? anchor : nil
      rescue
        nil
      end
    end

    def save_anchor(anchor)
      File.open("#{Rails.root}/config/anchor.txt", 'w'){ |f| f.puts anchor  } if anchor.present?
    end

    def search_from_3taps(*args)
      options = args.extract_options!
      options = search_options.merge(options)
      ThreetapsClient.search(options) || {}
    end

    def poll_from_3taps(*args)
      options = args.extract_options!
      options = poll_options.merge(options)
      ThreetapsClient.poll(options) || {}
    end

    def filter_3taps_posts(postings)
      normal_posts   = []
      priority_posts = []
      postings.each do |posting|
        posting['id'] = posting['id'].to_s
        annotations   = posting['annotations'] || {}
        email         = annotations['source_account']
        next if email.blank? || !email.match(Post::EMAIL_REGEX)

        posting['timestamp'] = annotations['original_posting_date'] if annotations['original_posting_date'].present?

        images = posting.delete('images') || []
        images = images.inject([]) do |ary, image|
          if image['full'].present?
            ary << image['full']
          else
            ary.concat image.values
          end
        end
        images.uniq!
        posting['images'] = images

        posting['email']   = email
        posting['heading'] = posting['heading'].to_s
        posting['body']    = posting['body'].to_s
        title, body        = posting['heading'], posting['body']

        if email.match(Post::VERIFIED_EMAIL_REGEX) || title.match(Post::VERIFIED_EMAIL_REGEX) || body.match(Post::VERIFIED_EMAIL_REGEX)
          priority_posts << posting
        else
          next unless FIELDS_3TAPS[ posting['category_group'] ]
          priority = 0
          priority -= 2 if posting['images'].present?
          priority -= 1 if keyword_regex && (title.match(keyword_regex) || body.match(keyword_regex))
          posting['priority'] = priority
          normal_posts << posting
        end
      end
      [priority_posts, normal_posts]
    end

    def import_prirority_posts(postings)
      postings.each do |posting|
        import_logger.info "-> priority posting = #{posting.except('body', 'annotations', 'images').inspect}"
        post = find_by_threetaps_id(posting['id']) || new
        if post.new_record?
          import_logger.info ".. Add new post (Images: #{posting['images'].size})"
        else
          import_logger.info ".. Update new post (Images: #{posting['images'].size})"
        end
        post.in_transaction{ |p| p.save_3taps_post(posting) }
        import_logger.info
      end
    end

    def import_normal_posts(postings, import_setting, setting)
      postings.each do |posting|
        import_logger.info "-> normal posting = #{posting.except('body', 'annotations', 'images').inspect}"
        post = find_by_threetaps_id(posting['id']) || new
        if post.new_record?
          if posting['deleted']
            import_logger.info ".. Post is new-deleted. Skip adding post"
            next
          end

          field = FIELDS_3TAPS[ posting['category_group'] ]
          if import_setting[field] >= setting[field]
            import_logger.info ".. Reached limit on #{posting['category_group'].inspect} category. Skip adding post"
          else
            import_logger.info ".. Add new post (Images: #{posting['images'].size})"
            post.in_transaction do |p|
              p.save_3taps_post(posting)
              import_setting[field] += 1
              import_setting.save!
            end
          end
        else
          import_logger.info ".. Update post (Images: #{posting['images'].size})"
          post.in_transaction{ |p| p.save_3taps_post(posting) }
        end
        import_logger.info
      end
    end

    def import_from_3taps
      import_logger.info "** Started at: #{Time.now}"
      setting        = Setting.first
      import_setting = ImportSetting.by_date!(Date.today)
      anchor         = read_anchor
      import_logger.info "=> Read anchor: #{anchor.inspect}"

      import_logger.info "=> Poll posts from 3taps"
      result = poll_from_3taps(:anchor => anchor)
      import_logger.info "=> Result from 3taps: #{result.except('postings').inspect}"

      anchor = result['anchor'] || anchor

      returning_postings = result['postings'] || []
      postings = returning_postings
      while returning_postings.present? do
        result = poll_from_3taps(:anchor => anchor)
        import_logger.info "=> Result from 3taps: #{result.except('postings').inspect}"
        returning_postings = result['postings'] || []
        anchor = result['anchor'] || anchor
        postings += returning_postings
      end

      if FIELDS_3TAPS.values.all?{ |f| import_setting.send(f) >= setting.send(f) }
        import_logger.info "=> Limitation is reached for today. Nothing to do, STOP!"
        import_logger.info "=> Save anchor: #{anchor.inspect}"
        save_anchor(anchor)
        return
      end

      import_logger.info "=> Filter #{postings.size} posts"
      priority_posts, normal_posts = filter_3taps_posts(postings)

      import_logger.info "=> Import priority posts: #{priority_posts.size}"
      import_prirority_posts(priority_posts)

      normal_posts = normal_posts.sort_by{ |posting| posting['priority'] }
      normal_posts = normal_posts[0..49]
      normal_posts = normal_posts[0..9] if Rails.env.development?

      import_logger.info "=> Import normal posts: #{normal_posts.size}"
      import_normal_posts(normal_posts, import_setting, setting)

      import_logger.info "=> Save anchor: #{anchor.inspect}"
      save_anchor(anchor)
      import_logger.info "** Finished at: #{Time.now}"
    rescue Interrupt
      import_logger.info "\n=> Exiting from Post.import_from_3taps!"
      exit 1
    rescue StandardError => ex
      save_anchor(anchor)
      import_logger.info "=> ERROR: #{ex.message}"
      Error.generate_from_exception!(ex)
    end

    def update_from_3taps(threetaps_ids=[])
      ThreetapsClient.log = nil
      threetaps_ids = Array(threetaps_ids)
      threetaps_ids = Post.imported.select('threetaps_id').map(&:threetaps_id) if threetaps_ids.blank?

      threetaps_ids.each do |id|
        result   = search_from_3taps :id => id
        postings = result['postings'] || []
        postings = filter_3taps_posts(postings).flatten

        postings.each do |posting|
          post = find_by_threetaps_id(posting['id'])
          next if post.blank?
          import_logger.info "=> Update post (ID: #{post.id}, 3taps ID: #{post.threetaps_id}): #{posting.except('body', 'annotations', 'images').inspect}"
          post.in_transaction{ |p| p.save_3taps_post(posting) }
          import_logger.info
        end
      end
      true
    rescue StandardError => ex
      import_logger.info "=> ERROR: #{ex.message}"
      Error.generate_from_exception!(ex)
    end

    def search_imported_posts(params={})
      arel = Post.imported
      arel = arel.search_by_params(params)
      arel.per_page(50)
    end
  end

  def self.included(receiver)
    receiver.extend ClassMethods
  end
end

