namespace :"3taps" do
  desc 'Cron postings from 3taps'
  task :cron => :environment do
    Post.import_from_3taps
  end
end
