def random_time
  (Time.now + [-1, 1].sample * rand(30).days).to_i
end

namespace :db do
  desc "Fill database with sample data"
  task :populate =>  :environment do |t, args|
    exit(0) unless Rails.env.development?
    posts = [
      {
        :name                    => "isn't my niece the cutest ever!!!",
        :body                    => "my niece!\n\n\n\n\n\nJe t'aime.",
        :email                   => "gwientjes@gmail.com",
        :category_id             => 8,
        :subcategory_id          => 130,
        :image_source1_file_name => "1193656956a.jpeg"
      },
      {
        :name                    => "I have a new niece!",
        :body                    => "I'm just letting everybody know that I have a new niece!",
        :email                   => "gwientjes@gmail.com",
        :category_id             => 9,
        :subcategory_id          => 92,
        :image_source2_file_name => "asha.jpeg"
      },
      {
        :name           => "Mountain bike for sale",
        :body           => "cheap bike",
        :email          => "gwientjes@gmail.com",
        :category_id    => 5,
        :subcategory_id => 4
      },
      {
        :name           => "1997 Toyota Camry for $2450",
        :body           => "Please buy my car.",
        :email          => "gwientjes@gmail.com",
        :category_id    => 5,
        :subcategory_id => 7
      }
    ]
    prefix_path = "#{Rails.root}/public/uploads/raw/"
    subcategories = SUBCATEGORIES.keys
    10.times do |n|
      posting = posts.sample

      subcategory_id = subcategories.sample
      category_id = SUBCATEGORIES[subcategory_id][:category_id]
      if posting[:image_source1_file_name]
        posting[:image_source1] = File.new(prefix_path + "1193656956a.jpeg", "r")
      end
      if posting[:image_source2_file_name]
        posting[:image_source2] = File.new(prefix_path + "asha.jpeg", "r")
      end
      posting[:category_id] =  category_id
      posting[:subcategory_id] = subcategory_id
      posting[:status] = 1

      posting[:time_posted] = random_time

      Post.new(posting).save :validate => false
    end
  end

  desc "Remove unused columns"
  task :remove_unused_columns => :environment do
    begin
      [:college_id, :category_name, :subcategory_name, :price, :skey].each do |column|
        ActiveRecord::Migration.remove_column :posts, column
      end

      puts "Done!"
    rescue Exception => ex
      puts ex.message
    end
  end
end

