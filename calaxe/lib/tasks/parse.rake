namespace :parse do
  desc 'Import data into parse.com - Options: NUM=20, TEST=1'
  task :import => :environment do
    ParseImporter.run(ENV)
  end
end
