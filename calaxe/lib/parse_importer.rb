require 'parse-ruby-client'

class ParseImporter
  MAX  = 100
  MIN  = 20
  KEYS = {
    :application_id => 'RpdufmbOLoCCFkUJ9lUHESu6Xb8pYJT8EJXpJ60v',
    :api_key        => 'gXjRIyoCdYiN3TWYafcU9ap0WsKN2xdkkunubZ77'
  }
  REMOVED_FIELDS = %w[
    id
    updated_at
    created_at
    image_source1_file_name
    image_source2_file_name
    image_source3_file_name
    image_source4_file_name
    external_id
    threetaps_id
  ]

  attr_reader :klass, :limit, :test, :debug

  def initialize(options = {})
    parse_options(options)
    setup
  end

  def logger
    self.class.logger
  end

  def run
    logger.info "** Started at: #{Time.now}"
    logger.info "** Finding #{limit} posts"
    logger.info "** Import data to #{klass.inspect} class"
    logger.info "** Running in TEST mode" if test

    posts = Post.all(:order => 'time_posted DESC', :limit => limit)
    if posts.empty?
      logger.info "** No posts, nothing to import. EXIT"
      logger.info "** Finished at #{Time.now}\n\n\n"
      return
    end

    posts         = posts.inject({}){ |h, p| h[p.id] = p; h }
    university_id = UNIVERSITIES[ SITE_CONFIG[:site_name].to_s.downcase ]

    unless university_id
      logger.error "** ERROR: university_id is nil. EXIT"
      return
    else
      logger.info "** University ID: #{university_id}"
    end

    new_posts, existing_posts = new_posts_for(posts.keys, university_id)
    if new_posts.empty?
      logger.info "** Posts exist in parse.com, nothing to import. EXIT"
      logger.info "** Finished at #{Time.now}\n\n\n"
      return
    else
      logger.info "** Existing posts #{existing_posts.inspect}"
      logger.info "** Importing #{new_posts.length} new posts"
    end

    if test
      new_posts.each{ |pid| logger.info "-> Imported Post ID=#{pid}" }
      return
    end

    new_posts.each do |id|
      post       = posts[id]
      attributes = post.attributes.merge('reference_id' => id, 'university_id' => university_id)
      REMOVED_FIELDS.each{ |field| attributes.delete(field) }

      (1..4).each do |idx|
        field = "image_source#{idx}"
        if post.send(field).blank?
          attributes.delete(field)
        else
          attributes[field] = post.send(field).url(:post)
        end
      end

      parse_post = Parse::Object.new klass, attributes
      parse_post.save
      logger.info "-> Imported Post ID=#{post.id}"
    end

    logger.info "** Finished at #{Time.now}\n\n\n"
  end

  def self.run(options = {})
    new(options).run
  rescue Exception => ex
    logger.info "** ERROR: #{ex.message}"
    ex.backtrace.each{ |caller| logger.error caller }
  end

  def self.logger
    @logger ||= ActiveSupport::BufferedLogger.new(Rails.env.development? ? STDOUT : 'log/parse.log')
  end

  protected

  def setup
    Parse.init(KEYS)
  end

  def parse_options(options)
    @klass = Rails.env.production? ? 'post' : 'posts'
    @limit = options['NUM'] ? options['NUM'].to_i : MIN
    if @limit > MAX
      @limit = MAX
    elsif @limit < MIN
      @limit = MIN
    end
    @test  = options['TEST'].present?
  end

  def new_posts_for(posts, university_id)
    logger.info "** Checking whether posts are imported to parse.com or not based on 'reference_id' and 'university_id'"
    query             = Parse::Query.new(klass)
    query.limit       = 1000
    query.order_by    = 'reference_id'
    query.order       = :descending
    query.eq('university_id', university_id)
    query.value_in('reference_id', posts)
    response          = query.get
    returning_posts   = response.map{ |o| o['reference_id'].to_i }
    existing_posts    = returning_posts & posts
    new_posts         = posts - existing_posts
    [new_posts, existing_posts]
  end
end
