# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Mayor.create(:name => 'Daley', :city => cities.first)

["nhphong1406@gmail.com", "gwientjes@gmail.com"].each do |e|
  User.create :email => e, :password => '123456', :password_confirmation => '123456'
end
