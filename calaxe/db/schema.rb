# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140524072119) do

  create_table "errors", :force => true do |t|
    t.datetime "time"
    t.text     "message"
    t.text     "backtrace"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "import_settings", :force => true do |t|
    t.date     "date"
    t.integer  "for_sale",   :default => 0
    t.integer  "housing",    :default => 0
    t.integer  "jobs",       :default => 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "import_settings", ["date"], :name => "index_import_settings_on_date"

  create_table "messages", :force => true do |t|
    t.text     "message"
    t.integer  "post_id"
    t.string   "ip"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "posts", :force => true do |t|
    t.integer  "category_id"
    t.integer  "subcategory_id"
    t.string   "email"
    t.string   "ip"
    t.text     "name"
    t.text     "body"
    t.integer  "security_id"
    t.integer  "time_posted"
    t.integer  "time_modified"
    t.string   "image_source1_file_name"
    t.string   "image_source2_file_name"
    t.string   "image_source3_file_name"
    t.string   "image_source4_file_name"
    t.integer  "status",                  :default => 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "threetaps_id"
    t.string   "external_id"
    t.string   "external_url"
    t.boolean  "featured"
  end

  add_index "posts", ["category_id"], :name => "index_posts_on_category_id"
  add_index "posts", ["name"], :name => "index_posts_on_name"
  add_index "posts", ["status"], :name => "index_posts_on_status"
  add_index "posts", ["subcategory_id"], :name => "index_posts_on_subcategory_id"
  add_index "posts", ["threetaps_id"], :name => "index_posts_on_threetaps_id"
  add_index "posts", ["time_posted"], :name => "index_posts_on_time_posted"

  create_table "settings", :force => true do |t|
    t.integer "for_sale"
    t.integer "housing"
    t.integer "jobs"
  end

  create_table "users", :force => true do |t|
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
