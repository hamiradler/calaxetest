class CreatePosts < ActiveRecord::Migration
  def self.up
    create_table :posts do |t|
      t.integer :category_id
      t.integer :subcategory_id
      t.string :email
      t.string :ip
      t.text :name
      t.text :body
      t.integer :security_id
      t.integer :time_posted
      t.integer :time_modified
      t.string :image_source1_file_name
      t.string :image_source2_file_name
      t.string :image_source3_file_name
      t.string :image_source4_file_name
      t.integer :status, :default => 0

      t.timestamps
    end

    add_index :posts, :name
    add_index :posts, :category_id
    add_index :posts, :subcategory_id
    add_index :posts, :time_posted
    add_index :posts, :status
  end

  def self.down
    drop_table :posts
  end
end

