class AddMoreColumnsToPosts < ActiveRecord::Migration
  def self.up
    add_column :posts, :threetaps_id,    :string
    add_column :posts, :external_id,     :string
    add_column :posts, :external_url,    :string

    add_index  :posts, :threetaps_id
  end

  def self.down
    remove_column :posts, :threetaps_id
    remove_column :posts, :external_id
    remove_column :posts, :external_url
  end
end
