class CreateSettings < ActiveRecord::Migration
  def self.up
    create_table :settings do |t|
      t.integer :for_sale
      t.integer :housing
      t.integer :jobs
    end

    Setting.create! :for_sale  => 700,
                    :housing   => 700,
                    :jobs      => 30
  end

  def self.down
    drop_table :settings
  end
end
