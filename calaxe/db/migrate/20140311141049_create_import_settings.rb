class CreateImportSettings < ActiveRecord::Migration
  def self.up
    create_table :import_settings do |t|
      t.date    :date
      t.integer :for_sale,  :default => 0
      t.integer :housing,   :default => 0
      t.integer :jobs,      :default => 0

      t.timestamps
    end

    add_index :import_settings, :date
  end

  def self.down
    drop_table :import_settings
  end
end
