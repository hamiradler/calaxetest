class ImportSetting < ActiveRecord::Base
  scope :ordered, order('date DESC')

  def self.by_date!(date=Date.today)
    setting = find_by_date(date)
    setting = create! :date => date unless setting
    setting
  end
end
