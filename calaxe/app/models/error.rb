class Error < ActiveRecord::Base
  scope :ordered, order('time DESC')

  def displayed_created_at
    created_at.strftime("%Y-%m-%d %H:%M:%S")
  end

  def displayed_time
    time.strftime("%Y-%m-%d %H:%M:%S")
  end

  def displayed_backtrace
    backtrace.gsub(/\n/, '<br>').html_safe
  end

  def name
    "##{id} - #{message}"
  end

  def self.generate_from_exception!(exception)
    create! :time => Time.now, :message => exception.message, :backtrace => exception.backtrace.join("\n")
  end
end
