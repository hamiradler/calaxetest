module GlobalConstants
  STANFORD_EMAIL = %w[stanford.edu stanfordgsb.org stanfordalumni.org]
  BERKELEY_EMAIL = %w[berkeley.edu]

  CAMPUS_JOBS_CATEGORY_ID = 1
  JOBS_CATEGORY_ID        = 2
  HOUSING_CATEGORY_ID     = 3
  FOR_SALE_CATEGORY_ID    = 5
  SERVICES_CATEGORY_ID    = 7
  PERSONALS_CATEGORY_ID   = 8
  COMMUNITY_CATEGORY_ID   = 9

  CATEGORIES_ORDER = [3, 5, 2, 8, 1, 9, 7]

  CATEGORIES_WITHOUT_JOBS_ORDER = [3, 5, 8, 9, 7]

  CATEGORIES = {
    5 => { :id  => 5, :name => 'for sale / wanted',  :short_name => 'for sale',
           :css => 'forsale',        :icon => 'for-sale-icon',        :column => 2 },
    8 => { :id  => 8, :name => 'personals / dating', :short_name => 'personals',
           :css => 'personals',      :icon => 'personals-icon',       :column => 1 },
    3 => { :id  => 3, :name => 'housing',   :short_name => 'housing',
           :css => 'housing',        :icon => 'housing-icon',         :column => 1 },
    2 => { :id  => 2, :name => 'off-campus jobs',  :short_name => 'jobs',
           :css => 'jobs_offcampus', :icon => 'off-campus-jobs-icon', :column => 1 },
    1 => { :id  => 1, :name => 'campus jobs',      :short_name => 'campus jobs',
           :css => 'jobs_campus',    :icon => 'campus-jobs-icon',     :column => 2 },
    9 => { :id  => 9, :name => 'community',        :short_name => 'community',
           :css => 'community',      :icon => 'community-icon',       :column => 2 },
    7 => { :id  => 7, :name => 'services offered', :short_name => 'services',
           :css => 'services',       :icon => 'services-icon',        :column => 2 }
  }

  CARS_SUBCATEGORY_ID = 7

  # For sale
  FOR_SALE_CATEGORY = {
    3   => { :id => 3,   :category_id => 5, :name => 'barter' },
    4   => { :id => 4,   :category_id => 5, :name => 'bicycles',               :name_home => "bikes" },
    6   => { :id => 6,   :category_id => 5, :name => 'books' },
    7   => { :id => 7,   :category_id => 5, :name => 'cars' },
    8   => { :id => 8,   :category_id => 5, :name => 'cds/dvds' },
    9   => { :id => 9,   :category_id => 5, :name => 'clothing & accessories', :name_home => "clothes+ac" },
    11  => { :id => 11,  :category_id => 5, :name => 'computers & tech',       :name_home => "computers" },
    12  => { :id => 12,  :category_id => 5, :name => 'electronics' },
    13  => { :id => 13,  :category_id => 5, :name => 'free stuff',             :name_home => "free" },
    14  => { :id => 14,  :category_id => 5, :name => 'furniture' },
    17  => { :id => 17,  :category_id => 5, :name => 'household items',        :name_home => "household" },
    18  => { :id => 18,  :category_id => 5, :name => 'items wanted',           :name_home => "wanted"},
    23  => { :id => 23,  :category_id => 5, :name => 'tickets' },
    151 => { :id => 151, :category_id => 5, :name => 'general' },
    155 => { :id => 155, :category_id => 5, :name => 'art + shop',             :name_home => "art+shop"},
    156 => { :id => 156, :category_id => 5, :name => 'games' }
  }

  FOR_SALE_CATEGORY_ARRAY = [
    { :id => 155, :category_id => 5, :name => 'art + shop',             :name_home => "art+shop"},
    { :id => 3,   :category_id => 5, :name => 'barter' },
    { :id => 4,   :category_id => 5, :name => 'bicycles',               :name_home => "bikes" },
    { :id => 6,   :category_id => 5, :name => 'books' },
    { :id => 7,   :category_id => 5, :name => 'cars' },
    { :id => 8,   :category_id => 5, :name => 'cds/dvds' },
    { :id => 9,   :category_id => 5, :name => 'clothing & accessories', :name_home => "clothes+ac" },
    { :id => 11,  :category_id => 5, :name => 'computers & tech',       :name_home => "computers" },
    { :id => 12,  :category_id => 5, :name => 'electronics' },
    { :id => 13,  :category_id => 5, :name => 'free stuff',             :name_home => "free" },
    { :id => 14,  :category_id => 5, :name => 'furniture' },
    { :id => 156, :category_id => 5, :name => 'games' },
    { :id => 17,  :category_id => 5, :name => 'household items',        :name_home => "household" },
    { :id => 18,  :category_id => 5, :name => 'items wanted',           :name_home => "wanted"},
    { :id => 23,  :category_id => 5, :name => 'tickets' },
    { :id => 151, :category_id => 5, :name => 'general' }
  ]

  # Personals
  PERSONALS_CATEGORY = {
    130 => { :id => 130, :category_id => 8, :name => 'friendship' },
    131 => { :id => 131, :category_id => 8, :name => 'girl wants girl' },
    132 => { :id => 132, :category_id => 8, :name => 'girl wants guy' },
    133 => { :id => 133, :category_id => 8, :name => 'guy wants girl' },
    134 => { :id => 134, :category_id => 8, :name => 'guy wants guy' },
    135 => { :id => 135, :category_id => 8, :name => 'general romance',        :name_home => "general" },
  }

  PERSONALS_CATEGORY_ARRAY = [
    { :id => 130, :category_id => 8, :name => 'friendship' },
    { :id => 131, :category_id => 8, :name => 'girl wants girl' },
    { :id => 132, :category_id => 8, :name => 'girl wants guy' },
    { :id => 133, :category_id => 8, :name => 'guy wants girl' },
    { :id => 134, :category_id => 8, :name => 'guy wants guy' },
    { :id => 135, :category_id => 8, :name => 'general romance',        :name_home => "general" },
  ]

  # Housing
  HOUSING_CATEGORY = {
    59  => { :id => 59,  :category_id => 3, :name => 'rooms & shares',         :name_home => "rooms/shared" },
    60  => { :id => 60,  :category_id => 3, :name => 'apartments for rent',    :name_home => "apts/housing" },
    66  => { :id => 66,  :category_id => 3, :name => 'sublets & temporary',    :name_home => "sublets/temporary" },
    68  => { :id => 68,  :category_id => 3, :name => 'apt/housing wanted',     :name_home => "housing wanted"  }
    # Update db, category_id changed to 3 from old housing wanted category_id
  }

  HOUSING_CATEGORY_ARRAY = [
    { :id => 60,  :category_id => 3, :name => 'apartments for rent',    :name_home => "apts/housing" },
    { :id => 68,  :category_id => 3, :name => 'apt/housing wanted',     :name_home => "housing wanted"  },
    { :id => 59,  :category_id => 3, :name => 'rooms & shares',         :name_home => "rooms/shared" },
    { :id => 66,  :category_id => 3, :name => 'sublets & temporary',    :name_home => "sublets/temporary" }
  ]

  # Jobs off-campus
  JOBS_CATEGORY = {
    158 => { :id => 158, :category_id => 2, :name => 'general' }
  }

  JOBS_CATEGORY_ARRAY = [
    { :id => 158, :category_id => 2, :name => 'general' }
  ]

  # Campus jobs
  CAMPUS_JOBS_CATEGORY = {
    141 => { :id => 141, :category_id => 1, :name => 'tutoring' },
    142 => { :id => 142, :category_id => 1, :name => 'teaching' },
    143 => { :id => 143, :category_id => 1, :name => 'admin' },
    144 => { :id => 144, :category_id => 1, :name => 'research' },
    146 => { :id => 146, :category_id => 1, :name => 'general' }
  }

  CAMPUS_JOBS_CATEGORY_ARRAY = [
    { :id => 143, :category_id => 1, :name => 'admin' },
    { :id => 144, :category_id => 1, :name => 'research' },
    { :id => 142, :category_id => 1, :name => 'teaching' },
    { :id => 141, :category_id => 1, :name => 'tutoring' },
    { :id => 146, :category_id => 1, :name => 'general' }
  ]

  # Community
  COMMUNITY_CATEGORY = {
    90  => { :id => 90,  :category_id => 9, :name => 'activity partners',      :name_home => "activities" },
    92  => { :id => 92,  :category_id => 9, :name => 'childcare' },
    93  => { :id => 93,  :category_id => 9, :name => 'general community',      :name_home => "general" },
    95  => { :id => 95,  :category_id => 9, :name => 'local news and views',   :name_home => "news+views" },
    96  => { :id => 96,  :category_id => 9, :name => 'lost & found',           :name_home => "lost+found" },
    101 => { :id => 101, :category_id => 9, :name => 'volunteers' },
    102 => { :id => 102, :category_id => 9, :name => 'classes' },
    149 => { :id => 149, :category_id => 9, :name => 'rideshare' }
  }

  COMMUNITY_CATEGORY_ARRAY = [
    { :id => 90,  :category_id => 9, :name => 'activity partners',      :name_home => "activities" },
    { :id => 92,  :category_id => 9, :name => 'childcare' },
    { :id => 102, :category_id => 9, :name => 'classes' },
    { :id => 96,  :category_id => 9, :name => 'lost & found',           :name_home => "lost+found" },
    { :id => 95,  :category_id => 9, :name => 'local news and views',   :name_home => "news+views" },
    { :id => 149, :category_id => 9, :name => 'rideshare' },
    { :id => 101, :category_id => 9, :name => 'volunteers' },
    { :id => 93,  :category_id => 9, :name => 'general community',      :name_home => "general" }
  ]

  # Services
  SERVICES_CATEGORY = {
    73  => { :id => 73,  :category_id => 7, :name => 'computer services',      :name_home => "computer" },
    80  => { :id => 80,  :category_id => 7, :name => 'tutoring' },
    147 => { :id => 147, :category_id => 7, :name => 'general' }
  }

  SERVICES_CATEGORY_ARRAY = [
    { :id => 73,  :category_id => 7, :name => 'computer services',      :name_home => "computer" },
    { :id => 80,  :category_id => 7, :name => 'tutoring' },
    { :id => 147, :category_id => 7, :name => 'general' }
  ]

  SUBCATEGORIES = {}.merge(FOR_SALE_CATEGORY).
                     merge(PERSONALS_CATEGORY).
                     merge(HOUSING_CATEGORY).
                     merge(JOBS_CATEGORY).
                     merge(CAMPUS_JOBS_CATEGORY).
                     merge(COMMUNITY_CATEGORY).
                     merge(SERVICES_CATEGORY)

  CATEGORY_SUBCATEGORIES_ARRAY = {
    5 => FOR_SALE_CATEGORY_ARRAY,
    8 => PERSONALS_CATEGORY_ARRAY,
    3 => HOUSING_CATEGORY_ARRAY,
    2 => JOBS_CATEGORY_ARRAY,
    1 => CAMPUS_JOBS_CATEGORY_ARRAY,
    9 => COMMUNITY_CATEGORY_ARRAY,
    7 => SERVICES_CATEGORY_ARRAY
  }

  # One catory has many subcategory
  CATEGORY_SUBCATEGORIES = {
    5 => FOR_SALE_CATEGORY,
    8 => PERSONALS_CATEGORY,
    3 => HOUSING_CATEGORY,
    2 => JOBS_CATEGORY,
    1 => CAMPUS_JOBS_CATEGORY,
    9 => COMMUNITY_CATEGORY,
    7 => SERVICES_CATEGORY
  }

  UNIVERSITIES = {
    'supost'       => 1,
    'beaverwall'   => 2,
    'calaxe'       => 3,
    'boilist'      => 4,
    'txanm'        => 5,
    'cuwal'        => 6,
    'trojy'        => 7,
    'cmtart'       => 8,
    'utxpost'      => 9,
    'brulist'      => 10,
    'uofilist'     => 11,
    'umwall'       => 12,
    'wislist'      => 13,
    'bigredlist'   => 14,
    'kingtrit'     => 15,
    'hoklist'      => 16,
    'pupost'       => 17,
    'huwall'       => 18,
    'yelljack'     => 19,
    'huskylist'    => 20,
    'bluejayslist' => 21,
    'buckeyeslist' => 22,
    'uflist'       => 23,
    'sundevlist'   => 24,
    'bobcatlist'   => 25
  }
end

