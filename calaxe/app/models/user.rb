class User < ActiveRecord::Base
  before_save :encrypt_password

  attr_accessible :email, :password, :password_confirmation
  attr_accessor :password


  validates_presence_of :email
  validates_uniqueness_of :email
  validates_presence_of :password, :on => :create
  validates_confirmation_of :password, :unless => lambda { |u| u.password.blank? }

  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.authenticate!(password)
      user
    else
      nil
    end
  end

  def authenticate!(password)
    self.password_hash == BCrypt::Engine.hash_secret(password, self.password_salt)
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(self.password, self.password_salt)
    end
  end
end
