class Message < ActiveRecord::Base
  before_validation :trim_spaces

  belongs_to :post

  validates :message, :presence => true

  validates :email,
            :format   => {
              :with => Post::EMAIL_REGEX,
              :message => "must be a valid email",
              :if => lambda { |m| m.email? }
            }

  # Instance methods

  def send_to_poster(post)
    NotifierMailer.msg(self, post).deliver
  end

  def info
    email? ? email : "Anonymous"
  end

  private

    def trim_spaces
      self.email = self.email.to_s.strip
    end

end

