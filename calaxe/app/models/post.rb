class Post < ActiveRecord::Base
  include ThreetapsIntegration

  # Filters
  before_validation :trim_spaces
  before_validation :assign_timestamp, :on => :create

  # Comment this line when importing data
  before_create :add_extra_information

  after_initialize :init

  # Class variables
  cattr_accessor :last_time

  attr_accessor :from_admin, :timestamp_type

  # Accessor, accessible, reader, writer
  attr_accessible :name, :body,           :email,
                         :category_id,    :subcategory_id, :ip,
                         :time_posted,    :status,
                         :image_source1,  :image_source2,
                         :image_source3,  :image_source4,
                         :image_source1_file_name, :image_source2_file_name,
                         :image_source3_file_name, :image_source4_file_name,
                         :timestamp_type, :featured

  # Associations
  has_many :messages

  def self.per_page
    15
  end

  # Constants
  STATUS_NEW         = -1
  STATUS_UNPUBLISHED = 0
  STATUS_ACTIVE      = 1
  STATUS_DELETED     = 2

  STATUSES = {
    STATUS_NEW         => "New",
    STATUS_UNPUBLISHED => "Unpublished",
    STATUS_ACTIVE      => "Active",
    STATUS_DELETED     => "Deleted"
  }

  NOW              = 0
  ONE_WEEK_RANDOM  = 1
  ONE_MONTH_RANDOM = 2

  TIMESTAMP_TYPES = {
    NOW              => "Now",
    ONE_WEEK_RANDOM  => "One week random interval",
    ONE_MONTH_RANDOM => "One month random with interval"
  }


  EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d.\-]+\.[a-z]+\z/i

  DOMAIN = SITE_CONFIG[:allowed_domains].map { |d| '[\w+\-.]+@' + Regexp.quote(d) }.join(')|(')

  WHITE_LIST_EMAILS = SITE_CONFIG[:white_list_emails].map { |e| "(#{Regexp.quote(e)})" }.join("|")

  if SITE_CONFIG[:allowed_subdomain]
    SUBDOMAIN = Array(SITE_CONFIG[:allowed_subdomain]).map do |d|
      '(' + '[\w+\-.]+@([a-z][a-z\d\-]*\.)+' + Regexp.quote(d) + ')'
    end.join('|')
    ACCEPTED_EMAIL_REGEX = /\A(#{DOMAIN})|(#{SUBDOMAIN})|#{WHITE_LIST_EMAILS}\z/i
    VERIFIED_EMAIL_REGEX = /\A(#{DOMAIN})|(#{SUBDOMAIN})\z/i
  else
    ACCEPTED_EMAIL_REGEX = /\A(#{DOMAIN})|#{WHITE_LIST_EMAILS}\z/i
    VERIFIED_EMAIL_REGEX = /\A(#{DOMAIN})\z/i
  end

  STYLES = {
    :post   => "340x700>", # width x height
    :ticker => "x120"
  }

  OPTIONS = {
    :styles         => STYLES,
    :default_url    => "/images/missing.png",
    :storage        => :s3,
    :s3_credentials => "#{Rails.root.to_s}/config/amazon_s3.yml",
    :path           => "posts/:id/:style_:basename.:extension"
  }

  TIME_FORMAT = "%a %b %d, %Y %I:%M %p"

  HAS_PHOTO_QUERY = [1, 2, 3, 4].map{ |i| "(#{self.table_name}.image_source#{i}_file_name IS NOT NULL)" }.join(" OR ")

  # Validations and callbacks
  validates :name, :presence => true, :length => { :maximum =>  71 }, :if => lambda{ |p|  !p.from_admin }
  validates :name, :presence => true, :length => { :maximum => 255 }, :if => lambda{ |p| !!p.from_admin }

  validates :body,
            :presence => true,
            :length   => { :maximum => 32000 }

  validates :email,
            :presence => true,
            :format   => {
              :with        => EMAIL_REGEX,
              :message     => "must be valid",
              :allow_nil   => true,
              :allow_blank => true
            }

  validates :category_id,
            :subcategory_id,
            :presence => true

  # has_attached_file :avatar, options
  has_attached_file :image_source1, OPTIONS
  has_attached_file :image_source2, OPTIONS
  has_attached_file :image_source3, OPTIONS
  has_attached_file :image_source4, OPTIONS

  # Named scope, scope
  # Search for posts with photos.
  scope :has_photo, where(HAS_PHOTO_QUERY)
  scope :active,
        lambda { where("status = ? AND time_posted <= ?", STATUS_ACTIVE, Time.now.to_i) }

  scope :future,
        lambda { where("status = ? AND time_posted > ?", STATUS_ACTIVE, Time.now.to_i) }
  scope :recent, order("time_posted DESC")
  scope :this_week,
        lambda { where("time_posted > ?", (Time.now - 7.days).to_i) }
  scope :category,
        lambda { |category_id| where(:category_id => category_id) }
  scope :subcategory,
        lambda { |subcategory_id| where(:subcategory_id => subcategory_id) }
  scope :query,
        lambda { |query| where("lower(name) LIKE ? OR lower(body) LIKE ?", "%#{query.downcase}%", "%#{query.downcase}%") }

  scope :order_by, lambda { |sort_type| order("time_posted #{sort_type}") }

  scope :imported, where('threetaps_id IS NOT NULL')
  scope :manual,   where('threetaps_id IS NULL')
  scope :featured, lambda{ active.where(:featured => true) }
  scope :normal, where('featured IS NULL OR featured IS FALSE')

  # Instance methods
  def from_craigslist?
    email.present? && email =~ /craigslist\.(com|org)$/
  end

  def product
    case self.category_id
    when CAMPUS_JOBS_CATEGORY_ID
      "campus-job"
    when JOBS_CATEGORY_ID
      "jobs"
    when HOUSING_CATEGORY_ID
      "housing"
    when FOR_SALE_CATEGORY_ID
      self.subcategory_id == CARS_SUBCATEGORY_ID ? "for-sale-cars" : "for-sale-non-cars"
    when SERVICES_CATEGORY_ID
      "services"
    when PERSONALS_CATEGORY_ID
      "personals"
    when COMMUNITY_CATEGORY_ID
      "community"
    else
      "for-sale-non-cars"
    end
  end

  def from_university?
    email =~ ACCEPTED_EMAIL_REGEX
  end

  def mark_as_unpublished
    self.update_attributes(:status => STATUS_UNPUBLISHED)
  end

  def activate
    self.update_attributes(:status => STATUS_ACTIVE) if self.unpublished? || self.deleted?
  end

  def republish
    self.update_attributes(:status => STATUS_ACTIVE)
  end

  def unpublish
    self.update_attributes(:status => STATUS_DELETED) if self.active?
  end

  def deliver_activate_mail
    NotifierMailer.activate(self).deliver
  end

  # Virtual Attributes

  def css
    CATEGORIES[category_id].try(:[], :css)
  end

  def date
    full_time.to_date
  end

  def full_date
    date.strftime("%b %d, %Y")
  end

  def short_date
    date.strftime("%a, %b %d")
  end

  def time
    full_time.strftime(TIME_FORMAT)
  end

  def full_time
    Time.at(time_posted)
  end

  def category
    CATEGORIES[category_id].try(:[], :short_name)
  end

  def subcategory
    SUBCATEGORIES[subcategory_id].try(:[], :name)
  end

  def status_name
    STATUSES[self.status]
  end

  def name_with_tag
    email.match(VERIFIED_EMAIL_REGEX) ? "#{name} #{SITE_CONFIG[:symbol_email]}" : name
  end

  def verified?
    !!email.match(VERIFIED_EMAIL_REGEX)
  end

  # Instance methods
  # Does this post have a photo or not?
  def has_photo?
    image_source1_file_name.present? || image_source2_file_name.present? || image_source3_file_name.present? || image_source4_file_name.present?
  end

  def new?
    status == STATUS_NEW
  end

  # Is this an active post?
  def active?
    status == STATUS_ACTIVE
  end

  def unpublished?
    status.nil? || status == STATUS_UNPUBLISHED
  end

  def deleted?
    status == STATUS_DELETED
  end

  def unavailable?
    new? || deleted? || unpublished?
  end

  def new_day?
    klass = self.class
    if klass.last_time.nil? || klass.last_time != date
      klass.last_time = date
      true
    else # klass.last_time == date # Same date
      false
    end
  end

  # Sample photo -- regardless of the image_source attribute originally
  # assigned by user
  def photo
    return image_source1.url(:ticker) unless image_source1_file_name.blank?
    return image_source2.url(:ticker) unless image_source2_file_name.blank?
    return image_source3.url(:ticker) unless image_source3_file_name.blank?
    return image_source4.url(:ticker) unless image_source4_file_name.blank?
  end

  # Return photos for this post as an array
  def photos
    photos = []
    photos << image_source1.url(:post) unless image_source1_file_name.blank?
    photos << image_source2.url(:post) unless image_source2_file_name.blank?
    photos << image_source3.url(:post) unless image_source3_file_name.blank?
    photos << image_source4.url(:post) unless image_source4_file_name.blank?
    photos
  end

  # Class methods
  def self.subcategories(category_id)
    CATEGORY_SUBCATEGORIES_ARRAY[category_id.to_i] || []
  end

  def self.category_activity_metrics
    activity = {}
    count_by_category = Post.category(CATEGORIES.keys).this_week.group(:category_id).count
    CATEGORIES_ORDER.each do |cid|
      post = Post.category(cid).active.recent.first

      activity[cid] = {
        #:count => category(cid).count_this_week,
        :count => count_by_category[cid] || 0,
        :recent_time => post.full_time
      } if post
    end
    activity
  end

  # Count the posts this week
  def self.count_this_week
    self.this_week.active.count
  end

  def self.verified_posts
    sql    = SITE_CONFIG[:allowed_domains].map{ |_| "email LIKE ?" }.join(' OR ')
    values = SITE_CONFIG[:allowed_domains].map{ |d| "%#{d}" }
    arel   = self.active.recent.normal
    arel   = arel.where([sql, *values])
    arel   = arel.where(["time_posted >= ?", (Time.now - 2.weeks).to_i])
    arel.limit(50)
  end

  # Search for posts based on search criteria specified within params
  def self.posts( params )
    limit = params[:controller] == "posts" ? 100 : 50

    if params[:cat] && params[:cat].to_i == JOBS_CATEGORY_ID
      finder = self.active.recent
    else
      finder = self.active.recent.normal
    end
    finder = finder.category(params[:cat]) unless params[:cat].nil?
    finder = finder.subcategory(params[:sub]) unless params[:sub].nil?
    finder = finder.query( params[:q] ) unless params[:q].nil?
    finder = finder.paginate(:page => params[:page], :per_page => limit)
  end

  # Find the 4 most recent posts with photos
  def self.find_recent_photos
    self.has_photo.recent.active.limit(4)
  end

  # This function is written to import data
  def self.create_with_id(attributes)
    post = Post.new(attributes)
    post.id = attributes[:id]
    post.save!(:validate => false)
  end

  def self.search_by_params(params)
    query = params[:query]

    arel  = self

    if query.present?
      field = params[:field]
      case field
      when 'name', 'threetaps_id', 'email'
        arel = arel.where(["#{field} LIKE ?", "%#{query}%"])
      when 'id'
        arel = arel.where(['id = ?', query.to_i])
      end
    end

    arel.recent.page(params[:page])
  end

  private

    def trim_spaces
      self.name = self.name.to_s.strip
      self.email = self.email.to_s.strip
    end

    def init
      self.from_admin = false
    end

    def assign_timestamp
      now = Time.now
      case timestamp_type.to_i
      when NOW
        self.time_posted = now.to_i
      when ONE_WEEK_RANDOM
        self.time_posted = random(now, :next_week)
      when ONE_MONTH_RANDOM
        self.time_posted = random(now, :next_month)
      end
    end

    # Fill in some fields for the post before saving the object to the database.
    # consider change this to before_create later - so no overwrite when perform update command (when post published or deleted)
    def add_extra_information
      self.time_modified = self.time_posted
      self.security_id = rand(99999)
      # self.status = 0
    end

    def random(now, period=:next_week)
      start_time = now.to_i
      end_time = now.send(period).to_i
      start_time + (end_time - start_time) * rand
    end

end

