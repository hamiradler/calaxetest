class Setting < ActiveRecord::Base
  validates :for_sale, :housing, :jobs,
            :presence     => true,
            :numericality => {
              :only_integer             => true,
              :greater_than_or_equal_to => 0,
              :less_than_or_equal_to    => 10000,
              :allow_blank              => true
            }
end
