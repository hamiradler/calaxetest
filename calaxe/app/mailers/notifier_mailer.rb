class NotifierMailer < ActionMailer::Base
  layout 'mailer'

  helper :application

  default :from => SITE_CONFIG[:noreply_email]

  def activate(post)
    @post = post
    options = {
      :to      => filter_email(@post.email),
      :subject => "#{SITE_CONFIG[:site_name]} - Publish your post! #{@post.name}"
    }
    options.merge! :bcc  => SITE_CONFIG[:bcc_email] if Rails.env.production?
    mail options
  end

  def msg(message, post)
    @message = message
    @post    = post
    options = {
      :to       => filter_email(@post.email),
      :reply_to => filter_email(@message.email),
      :subject  => "#{SITE_CONFIG[:site_name]} - #{@message.info} response: #{@post.name}"
    }
    options.merge! :bcc  => SITE_CONFIG[:bcc_email] if Rails.env.production?
    mail options
  end

  private

    def filter_email(email)
      if Rails.env.production?
        email
      else
        'nhphong1406@gmail.com'
      end
    end
end
