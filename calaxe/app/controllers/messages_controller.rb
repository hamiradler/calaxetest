class MessagesController < ApplicationController
  def create
    post    = Post.find(params[:post_id])
    message = post.messages.build(params[:message])
    message.ip      = request.remote_host

    if message.valid?
      flash[:notice] = "Message Sent!"
      flash[:saved]  = true
      message.send_to_poster(post)
    else
      flash[:message] = message.message
      flash[:email]  = message.email
      flash[:message_error] = message.errors[:message].first
      flash[:email_error] = message.errors[:email].first
    end

    redirect_to post
  end
end

