class PostsController < ApplicationController
  before_filter :set_last_time
  before_filter :find_post,                     :only => [:preview, :continue, :show]
  before_filter :find_secured_post,             :only => [:publish, :delete]
  before_filter :find_category_and_subcategory, :only => [:new, :create]

  def index
    @posts = Post.posts params
    @title = "Posts"
  end

  def new
    @title = "Post"
    @post  = new_post @category, @subcategory
  end

  def create
    @title = "Post"
    @post  = new_post @category, @subcategory

    if @category.present? && @subcategory.present? && @post.save
      update_session_for_post(@post)
      redirect_to preview_post_path(@post)
    else
      render "new"
    end
  end

  def preview
    @title = "Publish your post | #{@post.name}"
  end

  def continue
    redirect_to post_path(@post) and return unless @post.new?
    redirect_to root_path and return unless check_session_for_post?(@post)

    Post.transaction do
      @post.mark_as_unpublished
      unless @post.from_university?
        @post.activate
        expire_home_page
      end
      @post.deliver_activate_mail
    end

    update_session_for_post(nil)
    if @post.from_university?
      redirect_to instructions_posts_path
    else
      redirect_to post_path(@post)
    end
  end

  def show
    redirect_to root_path and return if @post.unavailable?
    @title   = @post.name
    @message = Message.new :message => flash[:message], :email => flash[:email]
  end

  def publish
    @post.republish
    expire_home_page
  end

  def delete
    @post.unpublish
    expire_home_page
  end

  def instructions
  end

  def featured
    @posts = Post.featured.recent.page(params[:page]).per_page(100)
    render "index"
  end

  private

  def update_session_for_post(post)
    session[:post_id]     = post.try(:id)
    session[:security_id] = post.try(:security_id)
  end

  def check_session_for_post?(post)
    return false if post.blank?
    session[:post_id] == post.id && session[:security_id] == post.security_id
  end

  def new_post(category, subcategory)
    Post.new(params[:post]) do |post|
      post.status = Post::STATUS_NEW
      post.ip     = request.remote_ip

      if category.present? && post.category_id.blank?
        post.category_id = category[:id]

        if subcategory.present? && post.subcategory_id.blank?
          post.subcategory_id = subcategory[:id]
        end
      elsif category.blank? && post.category_id.present? && CATEGORIES[post.category_id.to_i]
        category = CATEGORIES[post.category_id.to_i]

        if subcategory.blank? && category.present? && post.subcategory_id.present? && SUBCATEGORIES[post.subcategory_id.to_i]
          subcategory = SUBCATEGORIES[post.subcategory_id.to_i]
        end
      end
    end
  end

  def find_post
    @post = Post.find params[:id]
  end

  def find_secured_post
    @post = Post.find_by_id_and_security_id params[:id], params[:security_id]
  end

  def find_category_and_subcategory
    @category    = nil
    @subcategory = nil

    if params[:cat].present?
      @category = CATEGORIES[params[:cat].to_i]

      if @category.present? && params[:sub].present?
        @subcategory = SUBCATEGORIES[params[:sub].to_i]
      end
    end
  end

  def set_last_time
    Post.last_time = nil
  end
end

