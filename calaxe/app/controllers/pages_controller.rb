class PagesController < ApplicationController
  before_filter :set_title, :except => [:home]

  caches_page :home

  def home
    @categories = CATEGORIES
    @count_this_week = Post.count_this_week
    @category_activity = Post.category_activity_metrics
    @verified_posts = Post.verified_posts.all
    @posts = Post.posts(params).all - @verified_posts
    @posts = @verified_posts + @posts
    @photosRecentPosts = Post.find_recent_photos
    @featured_posts = Post.featured.recent
  end

  def about
  end

  def contact
  end

  def privacy
  end

  def terms
  end

  def help
  end

  private
  def set_title
    @title = params[:action].capitalize
  end
end

