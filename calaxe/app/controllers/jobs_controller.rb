class JobsController < ApplicationController
  before_filter :redirect_to_new_job_page

  def index
  end

  def subscriber
  end

  def unsubscribe
  end

  def success
  end

  private

    def redirect_to_new_job_page
      redirect_to SITE_CONFIG[:job_url] and return
    end
end

