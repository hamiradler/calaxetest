class ApplicationController < ActionController::Base
  protect_from_forgery

  if Rails.env.production?
    before_filter :mailer_set_url_options
  end

  private
  def expire_home_page
    expire_page :controller => :pages, :action => :home
  end

  def mailer_set_url_options
    ActionMailer::Base.default_url_options = { :host => request.host_with_port }
  end
end

