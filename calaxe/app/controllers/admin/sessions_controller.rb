class Admin::SessionsController < AdminController
  layout false

  def new
    session[:user_id] = nil
  end

  def create
    user = User.authenticate(params[:email], params[:password])
    if user
      session[:user_id] = user.id
      redirect_to admin_posts_path
    else
      flash[:alert] = "Email or password is incorrect"
      render 'new'
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to :action => :new
  end
end
