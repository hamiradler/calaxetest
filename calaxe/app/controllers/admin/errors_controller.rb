class Admin::ErrorsController < AdminController
  before_filter :login_required
  before_filter :find_error, :only => [:show, :edit, :destroy]

  def index
    @errors = Error.ordered
  end

  def show
  end

  def edit
    render :action => 'show'
  end

  def destroy
    @error.destroy
    redirect_to admin_errors_url
  end

  def remove
    if params[:clean_all_errors]
      Error.delete_all
    else
      Error.delete_all(['time < ?', Date.today - 5.days])
    end
    redirect_to admin_errors_url
  end

  private

    def find_error
      @error = Error.find params[:id]
    end
end
