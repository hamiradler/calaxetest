class Admin::SettingsController < AdminController
  before_filter :login_required

  def index
    @setting = Setting.first
  end

  def update
    @setting = Setting.first
    if @setting.update_attributes(params[:setting])
      redirect_to :action => 'index'
    else
      render :action => 'index'
    end
  end
end
