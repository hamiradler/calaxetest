class Admin::PostsController < AdminController
  before_filter :login_required
  before_filter :find_post, :only => [:show, :edit, :update]

  def index
    @posts = Post.manual.search_by_params(params)
  end

  def new
    @post = Post.new :status => Post::STATUS_ACTIVE, :timestamp_type => Post::ONE_WEEK_RANDOM
  end

  def create
    @post = Post.new params[:post]
    @post.from_admin = true

    if @post.save
      redirect_to admin_posts_path
    else
      render 'new'
    end
  end

  def show
    render 'edit'
  end

  def edit
  end

  def update
    @post.from_admin = true

    if @post.update_attributes(params[:post])
      redirect_to edit_admin_post_path(@post)
    else
      render @post
    end
  end

  def overview
    @sort_type = params[:sort] || "asc"
    @posts = Post.future.order_by(@sort_type).page(params[:page]).per_page(30).all
    @posts_by_date = @posts.group_by(&:date)
  end

  def imported
    @posts = Post.search_imported_posts(params)
  end

  def verified
  end

  private

    def find_post
      @post = Post.find params[:id]
    end
end
