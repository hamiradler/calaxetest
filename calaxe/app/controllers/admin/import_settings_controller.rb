class Admin::ImportSettingsController < AdminController
  before_filter :login_required

  def index
    @settings = ImportSetting.ordered
  end
end
