module ApplicationHelper
  SEPARATOR  = " &raquo; ".html_safe
  BASE_TITLE = "#{SITE_CONFIG[:site_name]}: #{SITE_CONFIG[:university]} Classifieds"

  def render_formstack(post)
    return if post.blank? || post.new_record? || !post.new?

    if Rails.env.production?
      partial = "/shared/formstack/production"
    else
      partial = "/shared/formstack/test"
    end
    render :partial => partial, :locals => { :post => post }
  end

  def formstack_options(post)
    {
      :Email          => post.email,
      :product        => post.product,
      :cluster        => "",
      :university     => SITE_CONFIG[:university],
      :site           => SITE_CONFIG[:site_url],
      :post_id        => post.id,
      :post_url       => publish_post_url(post.id, post.security_id),
      :category_id    => post.category_id,
      :category       => post.category,
      :subcategory_id => post.subcategory_id,
      :subcategory    => post.subcategory
    }
  end

  def formstack_query(post)
    formstack_options(post).map { |field, value| value.to_query(field) }.join("&")
  end

  def homepage_title
    SITE_CONFIG[:homepage_title] || SITE_CONFIG[:university]
  end

  def post_body_tag(body)
    html = simple_format body
    html = auto_link html, :html => { :target => '_blank' }
    html.html_safe
  end

  def pagination
    will_paginate( :page_links => false, :previous_label => "Previous",
                                         :next_label => "Next 100 Posts" )
  end

  def photos_tag(photos)
    all_photos = photos.map { |p| image_tag(p) }.join("").html_safe
    content_tag(:div, all_photos, :class => "photos") unless all_photos.empty?
  end

  def post_tag(post)
    html = [link_to(post.name, post)]
    html << content_tag(:span, SITE_CONFIG[:symbol_email], :class => 'verified') if post.verified?
    html << content_tag(:span, time_ago_in_words(post.full_time), :class => 'time_ago') if params[:controller] == "pages"
    html << image_tag("Camera.gif") if post.has_photo?
    html.join(' ').html_safe
  end

  def edu_tag(post)
    content_tag(:span, SITE_CONFIG[:symbol_email], :class => 'verified') if post.verified?
  end

  def posts_css(post)
    default = "forsale"
    return default if !post || (params[:cat].nil? && params[:sub].nil?) || params[:q]
    post.css
  end

  def posts_header(post)
    header = "I couldn't find any posts."
    query_is_blank = params[:q].blank?
    if post
      cat_is_blank   = params[:cat].blank?
      sub_is_blank   = params[:sub].blank?
      if cat_is_blank && sub_is_blank && query_is_blank
        if post.featured?
          header = "all featured posts"
        else
          header = "all posts"
        end
      elsif !cat_is_blank
        header = post.category
      elsif !sub_is_blank
        header = post.subcategory
      else
        header = "'#{params[:q]}' searched" unless query_is_blank
      end
    else
      header << " '#{params[:q]}' searched" unless query_is_blank
    end
    header
  end

  def category_overview_tag(category, activity)
    content_tag :li, :class => category[:icon] do
      category_title_link(category) + content_tag(:span, short_time_ago(activity))
    end
  end

  def category_tag(category, activity)
    html =  category_title_link(category)
    div  =  content_tag(:span, time_ago(activity), :class => 'time_category')
    div  << content_tag(:span, category_count(activity), :class => 'count_category')
    html << content_tag(:div, div, :class => 'activity')
    html
  end

  def category_title_link(category)
    link_to category[:short_name], posts_path(:cat => category[:id])
  end

  def subcategory_tag(subcategory)
    content_tag :li do
      link_to subcategory[:name_home] || subcategory[:name],
              posts_path(:sub => subcategory[:id])
    end
  end

  def recent_photo_tag(post)
    html =  link_to(image_tag(post.photo, :class => "round"), post)
    html << tag(:br)
    html << link_to(post.name.truncate(24), post)
    html << tag(:br)
    html << time_ago_in_words(post.full_time)
    content_tag :div, html
  end

  def buy_job_post_link(name = "post a job")
    link_to name, SITE_CONFIG[:job_url]
  end

  def show_breadcrumb(post)
    html = link_to(SITE_CONFIG[:site_name], root_path)
    add_crumb(html, SITE_CONFIG[:breadcrumb] || SITE_CONFIG[:university], root_path)

    sub_id = params[:sub].to_i
    cat_id = params[:cat].to_i
    if (subcategory = SUBCATEGORIES[sub_id])
      cat_id = subcategory[:category_id]
      category = CATEGORIES[cat_id]

      if post.try(:new_record?)
        add_crumb html, category[:short_name], add_posts_path(:cat => cat_id)
        add_crumb html, subcategory[:name],    add_posts_path(:cat => cat_id, :sub => sub_id)
      else
        add_crumb html, category[:short_name], posts_path(:cat => cat_id)
        add_crumb html, subcategory[:name], posts_path(:sub => sub_id)
      end
    elsif (category = CATEGORIES[cat_id])
      if post.try(:new_record?)
        add_crumb html, category[:short_name], add_posts_path(:cat => cat_id)
      else
        add_crumb html, category[:short_name], posts_path(:cat => cat_id)
      end
    elsif post && !post.new_record?
      add_crumb html, post.category, posts_path(:cat    => post.category_id)
      add_crumb html, post.subcategory, posts_path(:sub => post.subcategory_id)
    end
    html
  end

  # Return a title on a per-page basis.
  def title
    page_title =  "#{BASE_TITLE}"
    page_title << " | #{@title}" if @title
    page_title
  end

  def logo
    image_tag(SITE_CONFIG[:logo_url], :alt => SITE_CONFIG[:site_name])
  end

  private

  def add_crumb(container, name, link)
    container << SEPARATOR
    container << link_to(name, link)
  end

  def category_count(activity)
    activity.try(:[], :count) || ""
  end

  def time_ago(activity)
    if activity
      time_ago_in_words(activity[:recent_time])
    else
      ""
    end
  end

  def short_time_ago(activity)
    time = time_ago(activity)
    time.sub!("about ", "")
    time.sub!('over ',  '')
    time.sub!("less than a minute", "1 min")
    time.sub!("second", "sec")
    time.sub!("minute", "min")
    time.sub!("hour",   "hr")
    time.sub!('year',   'yr')
    time.sub!("month",  "mo")
    time
  end

  def sort_link(sort_type="asc")
    html = content_tag(:span, "Sort by Date: ", :class => :label)
    if sort_type == "desc"
      html << link_to("ASC", params.merge(:sort => "asc"))
      html << content_tag(:span, " | DESC")
    else
      html << content_tag(:span, "ASC | ")
      html << link_to("DESC", params.merge(:sort => "desc"))
    end
    html.html_safe
  end

  def show_pagination(collection, options={})
    content_tag :div, :class => "custom_pagination" do
      content_tag(:div, page_entries_info(collection), :class => "page_info") +
        will_paginate(collection, :inner_window => 2, :class => "pagination").to_s.html_safe
    end
  end

  def human_field_name(field, required=nil, klass=ActiveRecord::Base)
    [klass.human_attribute_name(field), required].reject{|e| e.blank?}.join("")
  end

end

