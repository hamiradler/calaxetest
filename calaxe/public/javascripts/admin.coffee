$(document).ready ->
  $('#option-template').template "optionTemplate"
  categoryContainer = $ '#post_category_id'
  categoryContainer.change (evt)->
    sender = evt.target
    subcategories = CSA["#{sender.value}"]
    options = []
    subcategoryContainer = $ '#post_subcategory_id'
    subcategoryContainer.html ''
    $.each subcategories, (index, element) ->
      element["selected"] = ""
      element["selected"] = "selected" if element.id is CSA.selectedSubcategory
      $.tmpl("optionTemplate", element).appendTo subcategoryContainer
      return
    return

  categoryContainer.trigger 'change'

  jQuery('#post_timestamp_posted').datetimepicker {
    ampm: true
    dateFormat: "D M dd, yy"
    timeFormat: "hh:mm TT"
    stepMinute: 1
  }

  return

