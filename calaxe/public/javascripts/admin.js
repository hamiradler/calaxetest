
$(document).ready(function() {
  var categoryContainer;
  $('#option-template').template("optionTemplate");
  categoryContainer = $('#post_category_id');
  categoryContainer.change(function(evt) {
    var options, sender, subcategories, subcategoryContainer;
    sender = evt.target;
    subcategories = CSA["" + sender.value];
    options = [];
    subcategoryContainer = $('#post_subcategory_id');
    subcategoryContainer.html('');
    $.each(subcategories, function(index, element) {
      element["selected"] = "";
      if (element.id === CSA.selectedSubcategory) element["selected"] = "selected";
      $.tmpl("optionTemplate", element).appendTo(subcategoryContainer);
    });
  });
  categoryContainer.trigger('change');
  jQuery('#post_timestamp_posted').datetimepicker({
    ampm: true,
    dateFormat: "D M dd, yy",
    timeFormat: "hh:mm TT",
    stepMinute: 1
  });
});
